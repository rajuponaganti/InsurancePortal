package com.aizant.ip;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.modelmapper.AbstractConverter;
import org.modelmapper.AbstractProvider;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.Provider;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class InsurancePortalApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(InsurancePortalApplication.class, args);
	}
	
	@Bean(name = "modelMapper")
	public ModelMapper getModelMapper() {

		Provider<java.sql.Date> localDateProvider = new AbstractProvider<java.sql.Date>() {

			@Override
			public java.sql.Date get() {
				return new java.sql.Date(new java.util.Date().getTime());
			}
		};

		Converter<String, java.sql.Date> toStringDate = new AbstractConverter<String, java.sql.Date>() {

			@Override
			protected java.sql.Date convert(String source) {
				Date date =null;
				if(source!=null) {
					java.util.Date utilDate=null;
					LocalDateTime plusDays=null;
					java.util.Date from =null;
					LocalDateTime ldt = LocalDateTime.parse(source, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"));
					if(ldt!=null) {
						ZoneId indianZone = ZoneId.of("Asia/Kolkata");
				        ZonedDateTime asiaZonedDateTime = ldt.atZone(indianZone);
				         utilDate = java.util.Date.from( asiaZonedDateTime.toInstant() );
				         LocalDateTime localDateTime = utilDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
				          plusDays = localDateTime.plusDays(1);
				           from = java.util.Date.from(plusDays.toLocalDate().atStartOfDay()
				        	      .atZone(ZoneId.systemDefault())
				        	      .toInstant());
					}
			        if(utilDate!=null) {
			        	 date = new java.sql.Date(from.getTime());
			        }	
				}
				
				return date;
			}
		};

		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		modelMapper.createTypeMap(String.class, java.sql.Date.class);
		modelMapper.addConverter(toStringDate);
		modelMapper.getTypeMap(String.class, java.sql.Date.class).setProvider(localDateProvider);

		return modelMapper;
	}

}
