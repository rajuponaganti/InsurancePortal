package com.aizant.ip.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.aizant.ip.user.IUserRepository;
import com.aizant.ip.user.User;



/**
 * Created by stephan on 20.03.16.
 */
@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {
	
	private static final Logger logger = LoggerFactory.getLogger(JwtUserDetailsServiceImpl.class);

	@Autowired
	private IUserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String userName) {
		logger.info("JwtUserDetailsServiceImpl : loadUserByUsername ()");
		try {
			User user = userRepository.findByUserName(userName);
			if (user == null) {
				throw new UsernameNotFoundException(String.format("No user found with username '%s'.", userName));
			} else {
				return JwtUserFactory.create(user);
			}
		} catch (Exception e) {
			logger.error("JwtUserDetailsServiceImpl : loadUserByUsername ()", e);
		}
		return null;
	}
}
