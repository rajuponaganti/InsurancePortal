package com.aizant.ip.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtUser implements UserDetails {

	private final Long id;
	private final String username;
	private final String firstname;
	private final String lastname;
	private final String password;
	private final String email;
	private final boolean enabled;
	private final String mobile;
	private final Collection<? extends GrantedAuthority> authorities;
	
	
	@JsonIgnore
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	

	@JsonIgnore
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	public JwtUser(Long id, String username, String firstname, String lastname, String password, String email,
			boolean enabled, String mobile,  Collection<? extends GrantedAuthority> authorities) {
		super();
		this.id = id;
		this.username = username;
		this.firstname = firstname;
		this.lastname = lastname;
		this.password = password;
		this.email = email;
		this.enabled = enabled;
		this.mobile = mobile;
		this.authorities = authorities;
	}
	

}
