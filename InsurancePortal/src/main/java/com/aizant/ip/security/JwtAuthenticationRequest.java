package com.aizant.ip.security;

import java.io.Serializable;

public class JwtAuthenticationRequest implements Serializable {

	private static final long serialVersionUID = -8445943548965154778L;
	private String userName;
	private String password;
	public JwtAuthenticationRequest() {
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public JwtAuthenticationRequest(String userName, String password) {
		super();
		this.userName = userName;
		this.password = password;
	}

	
}
