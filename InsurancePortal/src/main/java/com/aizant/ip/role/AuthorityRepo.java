package com.aizant.ip.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.aizant.ip.user.Authority;

@Repository
public interface AuthorityRepo extends JpaRepository<Authority, Long>, JpaSpecificationExecutor<Authority>{

}