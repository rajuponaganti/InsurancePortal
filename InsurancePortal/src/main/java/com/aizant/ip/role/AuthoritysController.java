package com.aizant.ip.role;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.aizant.ip.user.Authority;

@RestController
public class AuthoritysController {

	@Autowired
	private AuthorityRepo authorityRepo;
	
	@Autowired
	private ModelMapper mapper;
	
	@PostMapping("/Authority")
	public ResponseEntity<?> saveAuthority(@RequestBody Authority authority) {
		Boolean flag = null;
		if (authority != null) {
			flag = Objects.nonNull(authorityRepo.save(authority));
		}
		return ResponseEntity.ok(flag);

	}
	
	@PutMapping("/Authority/{id}")
	public ResponseEntity<?> updateAuthority(@RequestBody Map<String, String> AuthorityMap, @PathVariable Long id){
		Boolean flag = null;
		if(id!=null) {
			Optional<Authority> optional = authorityRepo.findById(id);
			if(optional.isPresent()) {
				Authority authority = optional.get();
				mapper.map(AuthorityMap, authority);
				 flag = Objects.nonNull(authorityRepo.save(authority));
			}
		}
		return ResponseEntity.ok(flag);
		
	}
	
	@GetMapping("/Authoritys")
	public ResponseEntity<?> getAllAuthoritys(){
		return ResponseEntity.ok(authorityRepo.findAll());
		
	}
	
	@GetMapping("/Authoritys/active")
	public ResponseEntity<?> getActiveAuthoritys(){
		return ResponseEntity.ok(authorityRepo.findAll((r, cq, cb)->cb.and(cb.equal(r.get("status"), true))));
		
	}
	
	@GetMapping("/Authority/{id}")
	public ResponseEntity<?> getAuthority(@PathVariable Long id){
		Optional<Authority> optional = authorityRepo.findById(id);
		return ResponseEntity.ok((optional.isPresent())?optional.get():null);
		
	}
	
	@DeleteMapping("/Authority/{id}")
	public ResponseEntity<?> deleteAuthority(@PathVariable Long id){
		Boolean flag = null;
		Optional<Authority> optional = authorityRepo.findById(id);
		if(optional.isPresent()) {
			Authority authority = optional.get();
			 flag = Objects.nonNull(authorityRepo.save(authority));
		}
		
		return ResponseEntity.ok(flag);
		
	}
}
