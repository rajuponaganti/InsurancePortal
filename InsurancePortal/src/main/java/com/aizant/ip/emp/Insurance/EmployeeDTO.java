package com.aizant.ip.emp.Insurance;

import lombok.Data;

@Data
public class EmployeeDTO {

	private String empId;

	private String empName;

	private String empGender;

	private String empDOB;

	private String spouseName;

	private String spouseDOB;

	private String maritalStatus;

	private Boolean havingChildrens;

	private String insuranceType;

	private Integer noOfChildrens;
	private Boolean isParentsnominatedForIns;

	
	private String fatherName;
	private String fatherDOB;
	private String fatherGender;

	private String motherName;
	private String motherDOB;
	private String motherGender;
 
}
