package com.aizant.ip.emp.Insurance;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Table(name = "tbl_emp_insurance_info")
@Data
public class EmployeeInsuranceInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "emp_id", length = 10, unique = true)
	private String empId;
	
	@Column(name = "emp_name", length = 100)
	private String empName;
	
	@Column(name = "is_parents_nominated_For_ins")
	private Boolean isParentsnominatedForIns;
	
	@Column(name = "father_name", length = 50)
	private String fatherName;
	@Column(name = "father_dob")
	private Date fatherDOB;
	@Column(name = "father_gender", length = 10)
	private String fatherGender;
	
	@Column(name = "mother_name", length = 50)
	private String motherName;
	@Column(name = "mother_dob")
	private Date motherDOB;
	@Column(name = "mother_gender", length = 10)
	private String motherGender;

	
	@Column(name = "spouse_name", length = 100)
	private String spouseName;
	
	@Column(name = "emp_dob")
	@Temporal(TemporalType.DATE)
	private Date empDOB;
	
	@Column(name = "spouse_dob")
	@Temporal(TemporalType.DATE)
	private Date spouseDOB;
	
	@Column(name = "marital_status", length = 20)
	private String maritalStatus;
	
	@Column(name = "emp_gender", length = 20)
	private String empGender;
	
	@Column(name = "insurance_type", length = 20)
	private String insuranceType;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "parent", fetch = FetchType.EAGER)
	private List<EmpChildren> empChildrens;
	
	private Boolean status;
	@Column(name = "having_children" )
	private Boolean havingChildrens;
	
	@Column(name = "no_of_childrens" )
	private Integer noOfChildrens;
	
}
