package com.aizant.ip.emp.Insurance;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IEmployeeInsuranceServiceImpl implements IEmployeeInsuranceService{
	private static final Logger logger = LoggerFactory.getLogger(IEmployeeInsuranceServiceImpl.class);
@Autowired	
private IEmployeeInsuranceRepo empInsRepo;


	
	@Override
	public Boolean saveEmployeeInsurance(EmployeeInsuranceDTO dto) {
		logger.info("In IEmployeeInsuranceServiceImpl : saveEmployeeInsurance () ");
		Boolean flag = null;
		try {
			EmployeeInsuranceInfo info2 =null;
			EmployeeInsuranceInfo info=new EmployeeInsuranceInfo();
			if(dto.getId()!=null) info.setId(dto.getId());
			EmployeeDTO empDetails = dto.getEmpDetails();
			if(empDetails !=null) {
				String empDOB = empDetails.getEmpDOB();
				if(empDOB!=null)info.setEmpDOB(getutilDate(empDOB));
				String spouseDOB = empDetails.getSpouseDOB();
				if(spouseDOB!=null)info.setSpouseDOB(getutilDate(spouseDOB));
				info.setEmpGender(empDetails.getEmpGender());
				info.setEmpId(empDetails.getEmpId());
				info.setEmpName(empDetails.getEmpName());
				info.setSpouseName(empDetails.getSpouseName());
				info.setMaritalStatus(empDetails.getMaritalStatus());
				info.setHavingChildrens(empDetails.getHavingChildrens());
				info.setNoOfChildrens(empDetails.getNoOfChildrens());
				info.setInsuranceType(empDetails.getInsuranceType());
				info.setIsParentsnominatedForIns(empDetails.getIsParentsnominatedForIns());
				if(dto.getId()!=null) {
					String fatherDOB = empDetails.getFatherDOB();
					if(fatherDOB!=null) info.setFatherDOB(getutilDate(fatherDOB));
					info.setFatherGender(empDetails.getFatherGender());
					info.setFatherName(empDetails.getFatherName());
					String motherDOB = empDetails.getMotherDOB();
					if(motherDOB!=null) info.setMotherDOB(getutilDate(motherDOB));
					info.setMotherGender(empDetails.getMotherGender());
					info.setMotherName(empDetails.getMotherName());
				}
				else {
					if(empDetails.getIsParentsnominatedForIns()!=null && empDetails.getIsParentsnominatedForIns()) {
						ParentDTO parentDetails = dto.getParentsDetails();
						if(parentDetails!=null) {
							String fatherDOB = parentDetails.getFatherDOB();
							if(fatherDOB!=null) info.setFatherDOB(getutilDate(fatherDOB));
							info.setFatherGender(parentDetails.getFatherGender());
							info.setFatherName(parentDetails.getFatherName());
							String motherDOB = parentDetails.getMotherDOB();
							if(motherDOB!=null) info.setMotherDOB(getutilDate(motherDOB));
							info.setMotherGender(parentDetails.getMotherGender());
							info.setMotherName(parentDetails.getMotherName());
						}
						
						
					}
				}
				info.setStatus(true);
				 //info2 = empInsRepo.save(info);
				getEmployeeInsuranceInfoWithChilds(dto, info);
				List<EmpChildren> empChildrens = info.getEmpChildrens();
				
				info.setEmpChildrens(empChildrens);
			}
			 info2 = empInsRepo.save(info);
			flag = Objects.nonNull(info2);
		} catch (Exception e) {
			logger.error("In IEmployeeInsuranceServiceImpl : saveEmployeeInsurance () ", e);
		}
		return flag;
	}
	public void  getEmployeeInsuranceInfoWithChilds(EmployeeInsuranceDTO dto , EmployeeInsuranceInfo parent){
		List<EmpChildren> childrens=new ArrayList<EmpChildren>();
		if(dto!=null && dto.getEmpDetails()!=null && dto.getEmpDetails().getNoOfChildrens()!=null && dto.getEmpDetails().getHavingChildrens()!=null && dto.getEmpDetails().getHavingChildrens()) {
			Map<String, Object> childrenDOBs = dto.getChildrenDOB();
			Map<String, Object> childrenGender = dto.getChildrenGender();
			Map<String, Object> childrenNames = dto.getChildrenNames();
			Map<String, Object> childrenIds = dto.getChildrenIds();
			for (int i = 1; i <= dto.getEmpDetails().getNoOfChildrens(); i++) {
				EmpChildren children=new EmpChildren();
				if(childrenIds!=null && childrenIds.size() > 0) {
					Integer id=(Integer ) childrenIds.get("childId "+i);
					if(id!=null)children.setId(Long.parseLong(id.toString()));
					
				}
				children.setName((String) childrenNames.get("childrenName "+i));
				children.setParent(parent);
				String gender = (String) childrenGender.get("childrenGender "+i);
				children.setGender(gender);
				String dob = (String) childrenDOBs.get("childrenDOB "+i);
				if(dob!=null) children.setDOB(getutilDate(dob));
				childrens.add(children);
			}
			
		}
		parent.setEmpChildrens(childrens);
	}
	
	public java.util.Date getutilDate(String source) {
		java.util.Date date=null;
		if(source!=null) {
			System.out.println(source);
			Instant instant = Instant.parse(source);
			ZonedDateTime z = instant.atZone(ZoneId.of("Asia/Kolkata"));
			date= Date.from(z.toInstant());
			System.out.println(date);
		}
		return date;
		
	}
	
	@Override
	public void exportToXLS(String empId, String all, HttpServletResponse response) {
		logger.info("In IEmployeeInsuranceServiceImpl : exportToXLS () ");
		try {
			List<EmployeeInsuranceExportDTO> list=new ArrayList<EmployeeInsuranceExportDTO>();
			if(empId!=null) {
				Optional<EmployeeInsuranceInfo> optional = empInsRepo.findOne((r, cq, cb)->cb.and(cb.equal(r.get("empId"), empId)));
				if(optional.isPresent()) {
					EmployeeInsuranceInfo info = optional.get();
					EmployeeInsuranceExportDTO exportDTO = getEmployeeInsuranceExportDTO(info);
					list.add(exportDTO);
				}
			}
			if(all!=null) {
				List<EmployeeInsuranceInfo> all2 = empInsRepo.findAll();
				for (EmployeeInsuranceInfo employeeInsuranceInfo : all2) {
					EmployeeInsuranceExportDTO exportDTO = getEmployeeInsuranceExportDTO(employeeInsuranceInfo);
					list.add(exportDTO);
				}
			}
			String currentDateTime = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new java.util.Date());
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFCellStyle style = getTextFormat(workbook);
			XSSFCellStyle dateStyle = getDateFormat(workbook);
			String[] aeColumns = { "Emp ID", "Name", "Gender", "DOB", "FatherName", "FatherDOB", "FatherGender","MotherName", "MotherDOB", "MotherGender", "Spouse Name",  "Spouse Dob", "Children 1 Name", "Children 1 Gender", "Children 1 Dob", "Children 2 Name", "Children 2 Gender", "Children 2 Dob", "Insurance Type"};
			response.setHeader("Content-Disposition", "attachment; filename=" + "EmpInsurance" + "_"+ currentDateTime +  ".xlsx");
			XSSFSheet aeSheet = getExcelSheet(workbook, aeColumns);
			int rowNum = 1;
			for (EmployeeInsuranceExportDTO dto : list) {
				Row row = aeSheet.createRow(rowNum++);
				setCellValueAndFormat(row.createCell(0), style, dto.getEmpId());
				setCellValueAndFormat(row.createCell(1), style, dto.getEmpName());
				setCellValueAndFormat(row.createCell(2), style, dto.getEmpGender());
				setCellValueAndFormat(row.createCell(3), dateStyle, dto.getEmpDOB());
				
				setCellValueAndFormat(row.createCell(4), style, dto.getFatherName());
				setCellValueAndFormat(row.createCell(5), dateStyle, dto.getFatherDOB());
				setCellValueAndFormat(row.createCell(6), style, dto.getFatherGender());
				
				setCellValueAndFormat(row.createCell(7), style, dto.getMotherName());
				setCellValueAndFormat(row.createCell(8), dateStyle, dto.getMotherDOB());
				setCellValueAndFormat(row.createCell(9), style, dto.getMotherGender());
				
				
				
				setCellValueAndFormat(row.createCell(10), style, dto.getSpouseName());
				setCellValueAndFormat(row.createCell(11), dateStyle, dto.getSpouseDOB());
				setCellValueAndFormat(row.createCell(12), style, dto.getFirstChildrenName());
				setCellValueAndFormat(row.createCell(13), style, dto.getFirstChildrenGender());
				setCellValueAndFormat(row.createCell(14), dateStyle, dto.getFirstChildrenDOB());
				setCellValueAndFormat(row.createCell(15), style, dto.getSecondChildrenName());
				setCellValueAndFormat(row.createCell(16), style, dto.getSecondChildrenGender());
				setCellValueAndFormat(row.createCell(17), dateStyle, dto.getSecondChildrenDOB());
				setCellValueAndFormat(row.createCell(18), style, dto.getInsuranceType());
				
			}
			closeOutStream(response, workbook);
		} catch (Exception e) {
			logger.error("In IEmployeeInsuranceServiceImpl : exportToXLS () ", e);
		}
		
	}
	private EmployeeInsuranceExportDTO getEmployeeInsuranceExportDTO(EmployeeInsuranceInfo info) {
		EmployeeInsuranceExportDTO dto=new EmployeeInsuranceExportDTO ();
		if(info!=null) {
			dto.setEmpId(info.getEmpId());
			dto.setEmpName(info.getEmpName());
			dto.setEmpGender(info.getEmpGender());
			dto.setEmpDOB(info.getEmpDOB());
			dto.setSpouseName(info.getSpouseName());
			dto.setSpouseDOB(info.getSpouseDOB());
			dto.setMaritalStatus(info.getMaritalStatus());
			dto.setInsuranceType(info.getInsuranceType());
			dto.setFatherDOB(info.getFatherDOB());
			dto.setFatherGender(info.getFatherGender());
			dto.setFatherName(info.getFatherName());
			dto.setMotherDOB(info.getMotherDOB());
			dto.setMotherGender(info.getMotherGender());
			dto.setMotherName(info.getMotherName());
			List<EmpChildren> empChildrens = info.getEmpChildrens();
			for (int i = 0; i < empChildrens.size(); i++) {
				EmpChildren empChildren = empChildrens.get(i);
				if(i==0) {
					dto.setFirstChildrenDOB(empChildren.getDOB());
					dto.setFirstChildrenGender(empChildren.getGender());
					dto.setFirstChildrenName(empChildren.getName());
				}
				if(i==1) {
					dto.setSecondChildrenDOB(empChildren.getDOB());
					dto.setSecondChildrenGender(empChildren.getGender());
					dto.setSecondChildrenName(empChildren.getName());
				}
				
			}
			
			
		}
		return dto;
	}
	
	private XSSFCellStyle getDateFormat(XSSFWorkbook workbook) {
		XSSFCellStyle dateStyle = workbook.createCellStyle();
		dateStyle.setDataFormat(workbook.createDataFormat().getFormat("dd-MMM-yyyy"));
		return dateStyle;
	}
	private XSSFCellStyle getTextFormat(XSSFWorkbook workbook) {
		XSSFCellStyle textStyle = workbook.createCellStyle();
		textStyle.setDataFormat(workbook.createDataFormat().getFormat("@"));
		return textStyle;
	}
	private void setCellValueAndFormat(Cell cell, XSSFCellStyle style,  java.util.Date date) {
		if(date!=null)cell.setCellValue(date);
		else cell.setCellValue("");
		cell.setCellStyle(style);
	}
	private void setCellValueAndFormat(Cell cell, XSSFCellStyle style, String value) {
		if(value!=null)cell.setCellValue(value);
		else cell.setCellValue("");
		cell.setCellStyle(style);
	}
	private XSSFSheet getExcelSheet(XSSFWorkbook workbook, String[] columns) {
		XSSFSheet sheet = workbook.createSheet("Sheet1");
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 11);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		headerFont.setFontName("Calibri");
		Row headerRow = sheet.createRow(0);
		XSSFCellStyle style = workbook.createCellStyle();
		style.setFont(headerFont);
		for (int i = 0; i < columns.length; i++) setCellValueAndFormat(headerRow.createCell(i), style, columns[i]);
		return sheet;
	}
	private void closeOutStream(HttpServletResponse response, XSSFWorkbook workbook) {
		try {
			ServletOutputStream outputStream = response.getOutputStream();
			workbook.write(outputStream);
			outputStream.close();
			workbook.close();
		} catch (IOException e) {
		}
	}
	@Override
	public EmployeeInsuranceDTO getEmployeeInsuranceInfo(Long id) {
		logger.info("In IEmployeeInsuranceServiceImpl : getEmployeeInsuranceInfo () ");
		EmployeeInsuranceDTO dto=new EmployeeInsuranceDTO();
		try {
			EmployeeDTO empDTO=new EmployeeDTO();
			Map<String, Object> childrenIds =new HashMap<String, Object> ();
			Map<String, Object> childrenNames =new HashMap<String, Object> ();
			Map<String, Object> childrenDOB = new HashMap<String, Object> ();
			Map<String, Object> childrenGender = new HashMap<String, Object> ();
			Optional<EmployeeInsuranceInfo> optional = empInsRepo.findById(id);
			if(optional.isPresent()) {
				EmployeeInsuranceInfo info = optional.get();
				dto.setEmpInsuranceInfoStatus(info.getStatus());
				if(info!=null) {
					dto.setId(id);
					empDTO.setEmpDOB((info.getEmpDOB()!=null)?info.getEmpDOB().toString():null);
					empDTO.setEmpGender(info.getEmpGender());
					empDTO.setEmpId(info.getEmpId());
					empDTO.setEmpName(info.getEmpName());
					String maritalStatus = info.getMaritalStatus();
					empDTO.setMaritalStatus(info.getMaritalStatus());
					if(maritalStatus !=null && maritalStatus.equalsIgnoreCase("married")) {
						empDTO.setSpouseDOB((info.getSpouseDOB()!=null)?info.getSpouseDOB().toString():null);
						empDTO.setSpouseName(info.getSpouseName());
							
					}
					
					empDTO.setNoOfChildrens(info.getNoOfChildrens());
					empDTO.setInsuranceType(info.getInsuranceType());
					empDTO.setHavingChildrens(info.getHavingChildrens());
					if(info.getHavingChildrens()!=null && info.getHavingChildrens()) {
						Integer noOfChildrens = info.getNoOfChildrens();
						dto.setNoOfChildrensArray(getIntegerArray(noOfChildrens));
						List<EmpChildren> empChildrens = info.getEmpChildrens();
						for (int i = 0; i < empChildrens.size(); i++) {
							Integer count=i+1;
							EmpChildren empChildren = empChildrens.get(i);
							if(empChildren!=null) {
								childrenIds.put("childId "+count, empChildren.getId());
								childrenNames.put("childrenName "+count, empChildren.getName());
								childrenDOB.put("childrenDOB "+count, empChildren.getDOB());
								childrenGender.put("childrenGender "+count, empChildren.getGender());
							}
						}
					}
					empDTO.setIsParentsnominatedForIns(info.getIsParentsnominatedForIns());
					if(info.getIsParentsnominatedForIns()!=null && info.getIsParentsnominatedForIns()) {
						empDTO.setFatherDOB((info.getFatherDOB() != null) ? info.getFatherDOB().toString() : null);
						empDTO.setFatherGender(info.getFatherGender());
						empDTO.setFatherName(info.getFatherName());
						empDTO.setMotherDOB((info.getMotherDOB() != null) ? info.getMotherDOB().toString() : null);
						empDTO.setMotherGender(info.getMotherGender());
						empDTO.setMotherName(info.getMotherName());
						 
					}
					dto.setEmpDetails(empDTO);
					dto.setChildrenIds(childrenIds);
					dto.setChildrenDOB(childrenDOB);
					dto.setChildrenGender(childrenGender);
					dto.setChildrenNames(childrenNames);
					
				}
			}
		} catch (Exception e) {
			logger.error("In IEmployeeInsuranceServiceImpl : getEmployeeInsuranceInfo () ", e);
		}
		return dto;
	}
	@Override
	public Boolean checkEmployeeIdExistence(String empId) {
		logger.info("In IEmployeeInsuranceServiceImpl : checkEmployeeIdExistence () ");
		Boolean flag = null;
		try {
			flag = Objects.nonNull(empInsRepo.findByEmpId(empId));
		} catch (Exception e) {
			logger.error("In IEmployeeInsuranceServiceImpl : checkEmployeeIdExistence () ", e);
		}
		return flag;
	}
	@Override
	public List<EmployeeInsuranceInfo> getAllEmployeeInsuranceInfo() {
		logger.info("In IEmployeeInsuranceServiceImpl : getAllEmployeeInsuranceInfo () ");
		List<EmployeeInsuranceInfo> list = null;
		try {
			list = empInsRepo.findAll();
		} catch (Exception e) {
			logger.error("In IEmployeeInsuranceServiceImpl : getAllEmployeeInsuranceInfo () ", e);
		}
		return list;
	}
	@Override
	public Boolean inactivateEmployeeInsuranceInfo(Long id) {
		logger.info("In IEmployeeInsuranceServiceImpl : inactivateEmployeeInsuranceInfo () ");
		Boolean flag=false;
		try {
			Optional<EmployeeInsuranceInfo> optional = empInsRepo.findById(id);
			if(optional.isPresent()) {
				EmployeeInsuranceInfo info = optional.get();
				info.setStatus(false);
				flag=Objects.nonNull(empInsRepo.save(info));
			}
		} catch (Exception e) {
			logger.error("In IEmployeeInsuranceServiceImpl : inactivateEmployeeInsuranceInfo () ", e);
		}
		return flag;
	}
	@Override
	public Boolean updateEmployeeInsuranceInfo(EmployeeInsuranceDTO dto) {
		logger.info("In IEmployeeInsuranceServiceImpl : updateEmployeeInsuranceInfo () ");
		Boolean flag=false;
		try {
			if(dto!=null && dto.getId()!=null) {
				EmployeeDTO empDetails = dto.getEmpDetails();
				Optional<EmployeeInsuranceInfo> optional = empInsRepo.findById(dto.getId());
				if (optional.isPresent()) {
					EmployeeInsuranceInfo info = optional.get();
					String maritalStatus = info.getMaritalStatus();
					String maritalStatus2 = empDetails.getMaritalStatus();
					Boolean havingChildrens = info.getHavingChildrens();
					Boolean havingChildrens2 = empDetails.getHavingChildrens();
					Integer noOfChildrens = info.getNoOfChildrens();
					Integer noOfChildrens2 = empDetails.getNoOfChildrens();
					// married to single
					if (maritalStatus != null && maritalStatus.equalsIgnoreCase("married") && maritalStatus2 != null && maritalStatus2.equalsIgnoreCase("single")) {
						// delete record and insert new record
						empInsRepo.deleteById(dto.getId());
					}
					// married with 2 children to 1 one or no children
					else if (havingChildrens!=null && havingChildrens2 != null && havingChildrens && !havingChildrens2) {
						if (noOfChildrens != null && noOfChildrens2 != null) {
							if (noOfChildrens2 < noOfChildrens)
								empInsRepo.deleteById(dto.getId());
						} else
							empInsRepo.deleteById(dto.getId());
						

					}
					flag = saveEmployeeInsurance(dto);

				}
					
					
			}
		} catch (Exception e) {
			logger.error("In IEmployeeInsuranceServiceImpl : updateEmployeeInsuranceInfo () ", e);
		}		
				
					 
		return flag;
	
	}
	public  Integer[] getIntegerArray(Integer num) {
		Integer[] array = null;
		if (num != null) {
			array = new Integer[num];
			Integer count = 0;
			for (int i = 0; i < num; i++) {
				count = count + 1;
				array[i] = count;
			}
		}
		return array;
	}
	@Override
	public Boolean activateEmployeeInsuranceInfo(Long id) {
		logger.info("In IEmployeeInsuranceServiceImpl : activateEmployeeInsuranceInfo () ");
		Boolean flag=false;
		try {
			Optional<EmployeeInsuranceInfo> optional = empInsRepo.findById(id);
			if(optional.isPresent()) {
				EmployeeInsuranceInfo info = optional.get();
				info.setStatus(true);
				flag=Objects.nonNull(empInsRepo.save(info));
			}
		} catch (Exception e) {
			logger.error("In IEmployeeInsuranceServiceImpl : activateEmployeeInsuranceInfo () ", e);
		}
		return flag;
	}
}
