package com.aizant.ip.emp.Insurance;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeInsuranceInfoController {
	@Autowired
	private IEmployeeInsuranceService empInsService;
	
	@PostMapping("/empInsSave")
	public ResponseEntity<Boolean> saveEmployeeInsuranceInfo(@RequestBody EmployeeInsuranceDTO dto){
		return ResponseEntity.ok(empInsService.saveEmployeeInsurance(dto));
		
	}
	@PutMapping("/empInsupdate")
	public ResponseEntity<Boolean> updateEmployeeInsuranceInfo(@RequestBody EmployeeInsuranceDTO dto){
		return ResponseEntity.ok(empInsService.updateEmployeeInsuranceInfo(dto));
		
	}
	
	@GetMapping("/empInfo/{id}")
	public ResponseEntity<?> getEmployeeInsuranceInfo(@PathVariable Long id){
		return ResponseEntity.ok(empInsService.getEmployeeInsuranceInfo(id));
		
	}
	
	
	@GetMapping("/empInfo/all")
	public ResponseEntity<?> getAllEmployeeInsuranceInfo(){
		return ResponseEntity.ok(empInsService.getAllEmployeeInsuranceInfo());
		
	}
	
	@GetMapping("/checkEmpId/{empId}")
	public ResponseEntity<?> checkEmployeeIdExistence(@PathVariable String empId){
		return ResponseEntity.ok(empInsService.checkEmployeeIdExistence(empId));
		
	}
	
	
	@GetMapping("/exportToXLS")
	public void exportToXLS(@RequestParam(required = false, name = "empId") String empId, @RequestParam(required = false, name = "all") String all , HttpServletResponse response){
		 empInsService.exportToXLS(empId, all, response);
		
	}
	
	@DeleteMapping("/empInfo/{id}")
	public ResponseEntity<?> inactivateEmployeeInsuranceInfo(@PathVariable Long id){
		return ResponseEntity.ok(empInsService.inactivateEmployeeInsuranceInfo(id));
		
	}
	
	@GetMapping("/empInfo/active/{id}")
	public ResponseEntity<?> activateEmployeeInsuranceInfo(@PathVariable Long id){
		return ResponseEntity.ok(empInsService.activateEmployeeInsuranceInfo(id));
		
	}
}
