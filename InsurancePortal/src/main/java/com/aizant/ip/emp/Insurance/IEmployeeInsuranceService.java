package com.aizant.ip.emp.Insurance;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

public interface IEmployeeInsuranceService {
	
	Boolean saveEmployeeInsurance(EmployeeInsuranceDTO dto);

	void exportToXLS(String empId, String all, HttpServletResponse response);

	EmployeeInsuranceDTO getEmployeeInsuranceInfo(Long id);

	Boolean checkEmployeeIdExistence(String empId);

	List<EmployeeInsuranceInfo> getAllEmployeeInsuranceInfo();

	Boolean inactivateEmployeeInsuranceInfo(Long id);

	Boolean updateEmployeeInsuranceInfo(EmployeeInsuranceDTO dto);

	Boolean activateEmployeeInsuranceInfo(Long id);

}
