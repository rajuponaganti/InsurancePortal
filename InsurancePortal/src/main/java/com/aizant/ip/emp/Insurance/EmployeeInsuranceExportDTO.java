package com.aizant.ip.emp.Insurance;

import java.util.Date;

import lombok.Data;

@Data
public class EmployeeInsuranceExportDTO {

	private String empId;

	private String empName;

	private String empGender;

	private Date empDOB;

	private String spouseName;

	private Date spouseDOB;

	private String maritalStatus;

	private Boolean havingChildrens;

	private String insuranceType;
	
	private String firstChildrenName;
	private String firstChildrenGender;
	private Date  firstChildrenDOB;
	private String secondChildrenName;
	private String secondChildrenGender;
	private Date  secondChildrenDOB;
	private String fatherName;
	private Date fatherDOB;
	private String fatherGender;

	private String motherName;
	private Date motherDOB;
	private String motherGender;
	

	
	
}
