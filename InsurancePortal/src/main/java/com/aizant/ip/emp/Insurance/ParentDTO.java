package com.aizant.ip.emp.Insurance;

import lombok.Data;

@Data
public class ParentDTO {

	private String fatherName;
	private String fatherDOB;
	private String fatherGender;

	private String motherName;
	private String motherDOB;
	private String motherGender;

}
