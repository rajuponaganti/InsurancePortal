package com.aizant.ip.emp.Insurance;

import java.util.Map;

import lombok.Data;

@Data
public class EmployeeInsuranceDTO {
	private Long id;
	private EmployeeDTO empDetails;
	
	private ParentDTO parentsDetails;
	
	private Map<String, Object> childrenNames;
	private Map<String, Object> childrenDOB;
	private Map<String, Object> childrenGender;
	private Map<String, Object> childrenIds;
	private Integer[] noOfChildrensArray;
	private Boolean empInsuranceInfoStatus;
	
	
}
