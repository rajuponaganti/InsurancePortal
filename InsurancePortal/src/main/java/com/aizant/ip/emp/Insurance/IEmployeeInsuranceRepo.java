package com.aizant.ip.emp.Insurance;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface IEmployeeInsuranceRepo extends JpaRepository<EmployeeInsuranceInfo, Long>,JpaSpecificationExecutor<EmployeeInsuranceInfo>{

	EmployeeInsuranceInfo findByEmpId(String empId);

}
