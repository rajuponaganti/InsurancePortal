package com.aizant.ip.emp.Insurance;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "tbl_emp_children")
@Data
public class EmpChildren {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long id;

	private String name;

	private String gender;

	@Column(name = "dob")
	@Temporal(TemporalType.DATE)
	private Date DOB;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id", referencedColumnName = "id")
	private EmployeeInsuranceInfo parent;

}
