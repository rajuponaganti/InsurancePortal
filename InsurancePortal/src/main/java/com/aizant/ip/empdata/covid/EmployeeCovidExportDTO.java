package com.aizant.ip.empdata.covid;

import lombok.Getter;
import lombok.Setter;
@Setter
@Getter
public class EmployeeCovidExportDTO {
private Integer empCode;
private String empName;
private String department;
private String vaccinationStatusSelf;
private String vaccinationStatusFamily;
private String vaccinatedwith;
private String doseAdministered;
private String ProofOfVaccination;
}
