package com.aizant.ip.empdata.covid;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/empCovid")
public class EmployeeCovidController {
	@Autowired
	private IEmployeeCovidService employeeCovidService;
	//save
	//http://localhost:8080/empCovid
	@PostMapping
	public ResponseEntity<?> saveEmployeeCovidData(@RequestBody EmployeeCovidDataRequest req){
		return employeeCovidService.saveEmployeeCovidData(req);
		
	}
	
	@PutMapping("/update")
	public ResponseEntity<?> updateEmployeeCovidData(@RequestBody EmployeeCovidDataRequest req){
		return employeeCovidService.updateEmployeeCovidData(req);
		
	}
	
	//get by id for update
	//http://localhost:8080/empCovid/122
	@GetMapping("/{id}")
	public ResponseEntity<?> getEmployeeCovidData(@PathVariable Long id){
		return employeeCovidService.getEmployeeCovidData(id);
		
	}
	//export url
	@GetMapping("/exportToXLS")
	public void exportToXLS(HttpServletResponse response){
		employeeCovidService.exportToXLS(response);
		
	}
	
	//get all for view
	//http://localhost:8080/empCovid
	@GetMapping()
	public ResponseEntity<?> getAllEmployeesCovidData(){
		return employeeCovidService.getAllEmployeesCovidData();
		
	}
	
	
}
