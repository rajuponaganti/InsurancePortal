package com.aizant.ip.empdata;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.aizant.ip.IPResponce;
import com.aizant.ip.empdata.covid.EmployeeCovidDataRequest;
import com.aizant.ip.empdata.covid.EmployeeVaccinationStatus;
import com.aizant.ip.empdata.covid.IEmployeeCovidService;
import com.aizant.ip.empdata.covid.IEmployeeVaccinationStatusRepository;
import com.aizant.ip.util.IPConstants;

@Service
public class EmployeeDataServiceImpl implements IEmployeeDataService{
	
	private static final Logger logger = LoggerFactory.getLogger(EmployeeDataServiceImpl.class);

	@Autowired
	private IEmployeeDataRepository employeeDataRepository;
	
	@Autowired
	private IEmployeeVaccinationStatusRepository empVaccStatusRepository;
	
	@Autowired
	private IEmployeeCovidService IEmployeeCovidService;
	
	@Override
	public ResponseEntity<?> saveEmployeeData(EmployeeData employeeData) {
		logger.info("In EmployeeDataServiceImpl : saveEmployeeData() ");
		ResponseEntity<?> responseEntity = null;
		try {
			responseEntity = ResponseEntity.ok(new IPResponce(Objects.nonNull(employeeDataRepository.save(employeeData))) );
		} catch (Exception e) {
			logger.error("In EmployeeDataServiceImpl : saveEmployeeData() ",e);
			responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new IPResponce("internal server error"));
		}
		return responseEntity;
	}

	/*
	 * @Override public ResponseEntity<?> updateEmployeeData(Map<String, Object>
	 * employeeData) { // TODO Auto-generated method stub return null; }
	 */

	@Override
	public ResponseEntity<?> getEmployeesData() {
		logger.info("In EmployeeDataServiceImpl : getEmployeesData() ");
		ResponseEntity<?> responseEntity = null;
		try {
			responseEntity = ResponseEntity.ok(employeeDataRepository.findAll());
		} catch (Exception e) {
			logger.error("In EmployeeDataServiceImpl : getEmployeesData() ", e);
			responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new IPResponce(IPConstants.INTERNAL_SERVER_ERROR));
		}
		return responseEntity;
	}

	@Override
	public ResponseEntity<?> getEmployeeData(Long id) {
		logger.info("In EmployeeDataServiceImpl : getEmployeeData() ");
		ResponseEntity<?> responseEntity = null;
		try {
			Optional<EmployeeData> optional = employeeDataRepository.findById(id);
			EmployeeData employeeData = (optional.isPresent())?optional.get():null;
			responseEntity = ResponseEntity.ok(employeeData);
		} catch (Exception e) {
			logger.error("In EmployeeDataServiceImpl : getEmployeeData() ", e);
			responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new IPResponce(IPConstants.INTERNAL_SERVER_ERROR));
		}
		return responseEntity;
	}

	@Override
	public Object InactivateEmployeeData(Long id) {
		logger.info("In EmployeeDataServiceImpl : InactivateEmployeeData () ");
		ResponseEntity<?> responseEntity = null;
		try {
			Optional<EmployeeData> optional = employeeDataRepository.findById(id);
			EmployeeData employeeData = (optional.isPresent())?optional.get():null;
			employeeData.setStatus(false);
			responseEntity = ResponseEntity.ok(new IPResponce(Objects.nonNull(employeeDataRepository.save(employeeData))) );
		} catch (Exception e) {
			logger.error("In EmployeeDataServiceImpl : InactivateEmployeeData () ", e);
			responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new IPResponce(IPConstants.INTERNAL_SERVER_ERROR));
		}
		return responseEntity;
	}

	@Override
	public ResponseEntity<?> isEmployeeExistedEmpCode(Integer empCode) {
		logger.info("In EmployeeDataServiceImpl : isEmployeeExistedEmpCode () ");
		Boolean flag = null;
		try {
			flag = employeeDataRepository.findOne((r, cq, cb)->cb.and(cb.equal(r.get("empCode"), empCode))).isPresent();
		} catch (Exception e) {
			logger.error("In EmployeeDataServiceImpl : isEmployeeExistedEmpCode () ", e);
		}
		
		return ResponseEntity.ok(flag);
	}

	@Override
	public ResponseEntity<?> getEmployeesDetails(String empCode) {
		logger.info("In EmployeeDataServiceImpl : getEmployeesDetails () ");
		EmployeeCovidDataRequest req = new EmployeeCovidDataRequest ();
		try {
			if(empCode!=null) {
				String trim = empCode.trim();
				if( trim=="" || trim.isEmpty()) {
					return ResponseEntity.ok(new IPResponce("Enter valid Emp Code"));
				}
				else {
					Integer eCode = Integer.parseInt(empCode);
					Optional<EmployeeData> optional = employeeDataRepository.findOne((r, cq, cb)->cb.and(cb.equal(r.get("empCode"), eCode)));
					EmployeeData empData = (optional.isPresent())?optional.get():null;
					if(empData!=null) {
						req.setIsExistedEmployee(true);
						req.setEmployeeData(empData);
						Optional<EmployeeVaccinationStatus> optional2 = empVaccStatusRepository.findOne((r, cq, cb)->cb.and(cb.equal(r.join("data").get("id"), empData.getId())));
						EmployeeVaccinationStatus empVaccStatus = (optional2.isPresent())?optional2.get():null;
						if(empVaccStatus!=null) {
							IEmployeeCovidService.getEmpCovidDetailsByIdUtils(empVaccStatus, req);
							
							req.setIsCovidRegistered(true);
						}  
						 else req.setIsCovidRegistered(false);
						 
					}
					else  req.setIsExistedEmployee(false);	
				}
				
				
				
			}
		} catch (NumberFormatException e) {
			logger.error("In EmployeeDataServiceImpl : getEmployeesDetails () ", e);
		} catch (Exception e) {
			logger.error("In EmployeeDataServiceImpl : getEmployeesDetails () ", e);
		}
		return ResponseEntity.ok(req);
	}

	@Override
	public void exportToXLS(HttpServletResponse response) {
		logger.info("In EmployeeDataServiceImpl : exportToXLS () ");
		try {
			List<EmployeeData> list = employeeDataRepository.findAll();
			String currentDateTime = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new java.util.Date());
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFCellStyle textStyle = getTextFormat(workbook);
			XSSFCellStyle numStyle = getNumberFormat(workbook);
			String[] aeColumns = { "E.code", "E.Name", "Department", "Designation", "Employee Pan Number", "Employee Aadhar  Number", "Highest  Qualification", "Personal Mobile Number",
					"Offical Mobile Number", "Company email ID", "Emergency Contact Person name", "Emergency Contact Person number", "Reporting Manager E.code",
					"Reporting Manager  Name", "Reporting Manager  Department", "Project  Working on", "Blood Group"};
			response.setHeader("Content-Disposition", "attachment; filename=" + "Employee_Details" + "_"+ currentDateTime +  ".xlsx");
			XSSFSheet aeSheet = getExcelSheet(workbook, aeColumns);
			int rowNum = 1;		
			for (EmployeeData dto : list) {
				Row row = aeSheet.createRow(rowNum++);
				setCellValueAndFormat(row.createCell(0), numStyle, dto.getEmpCode());
				setCellValueAndFormat(row.createCell(1), textStyle, dto.getEmpName());
				setCellValueAndFormat(row.createCell(2), textStyle, dto.getEmpDepartment());
				setCellValueAndFormat(row.createCell(3), textStyle, dto.getDesignation());
				
				setCellValueAndFormat(row.createCell(4), textStyle, dto.getEmployeePanNumber());
				setCellValueAndFormat(row.createCell(5), numStyle, dto.getEmpAadharNumber());
				setCellValueAndFormat(row.createCell(6), textStyle, dto.getHighestQualification());
				
				setCellValueAndFormat(row.createCell(7), numStyle, dto.getPersonalMobileNumber());
				setCellValueAndFormat(row.createCell(8), numStyle, dto.getOfficalMobileNumber());
				setCellValueAndFormat(row.createCell(9), textStyle, dto.getCompanyemailId());
				
				
				
				setCellValueAndFormat(row.createCell(10), textStyle, dto.getEmergencyContactPersonName());
				setCellValueAndFormat(row.createCell(11), numStyle, dto.getEmergencyContactPersonNumber());
				setCellValueAndFormat(row.createCell(12), numStyle, dto.getReportingManagerEmpCode());
				setCellValueAndFormat(row.createCell(13), textStyle, dto.getReportingManagerName());
				setCellValueAndFormat(row.createCell(14), textStyle, dto.getReportingManagerDepartment());
				setCellValueAndFormat(row.createCell(15), textStyle, dto.getProjectWorkingOn());
				setCellValueAndFormat(row.createCell(16), textStyle, dto.getBloodGroup());
				
			}
			closeOutStream(response, workbook);
		} catch (Exception e) {
			logger.error("In EmployeeDataServiceImpl : exportToXLS () ", e);
		}
		
	}
	private XSSFCellStyle getTextFormat(XSSFWorkbook workbook) {
		XSSFCellStyle textStyle = workbook.createCellStyle();
		textStyle.setDataFormat(workbook.createDataFormat().getFormat("@"));
		return textStyle;
	}
	private void setCellValueAndFormat(Cell cell, XSSFCellStyle style, String value) {
		if(value!=null)cell.setCellValue(value);
		else cell.setCellValue("");
		cell.setCellStyle(style);
	}
	private XSSFSheet getExcelSheet(XSSFWorkbook workbook, String[] columns) {
		XSSFSheet sheet = workbook.createSheet("Sheet1");
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 11);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		headerFont.setFontName("Calibri");
		Row headerRow = sheet.createRow(0);
		XSSFCellStyle style = workbook.createCellStyle();
		style.setFont(headerFont);
		for (int i = 0; i < columns.length; i++) setCellValueAndFormat(headerRow.createCell(i), style, columns[i]);
		return sheet;
	}
	private XSSFCellStyle getNumberFormat(XSSFWorkbook workbook) {
		XSSFCellStyle numberStyle = workbook.createCellStyle();
		numberStyle.setDataFormat(workbook.createDataFormat().getFormat("0"));
		return numberStyle;
	}
	private void setCellValueAndFormat(Cell cell, XSSFCellStyle style,  Number number) {
		if(number!=null) {
			if(number instanceof Integer) cell.setCellValue(number.intValue());
			else if(number instanceof Double) cell.setCellValue(number.doubleValue());
			else if(number instanceof Float) cell.setCellValue(number.floatValue());
			else if(number instanceof Byte) cell.setCellValue(number.byteValue());
			else if(number instanceof Short) cell.setCellValue(number.shortValue());
			else if(number instanceof Long) cell.setCellValue(number.longValue());	
		}
		else cell.setCellValue("");
		cell.setCellStyle(style);
	}
	private void closeOutStream(HttpServletResponse response, XSSFWorkbook workbook) {
		try {
			ServletOutputStream outputStream = response.getOutputStream();
			workbook.write(outputStream);
			outputStream.close();
			workbook.close();
		} catch (IOException e) {
		}
	}

}
