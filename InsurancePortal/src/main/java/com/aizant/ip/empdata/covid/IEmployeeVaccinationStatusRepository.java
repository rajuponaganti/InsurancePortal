package com.aizant.ip.empdata.covid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface IEmployeeVaccinationStatusRepository extends JpaRepository<EmployeeVaccinationStatus, Long>, JpaSpecificationExecutor<EmployeeVaccinationStatus>{

}
