package com.aizant.ip.empdata.covid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeCovidDoseAdministrationRepository extends JpaRepository<EmployeCovidDoseAdministration, Long>{

}
