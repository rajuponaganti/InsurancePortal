package com.aizant.ip.empdata.covid;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.aizant.ip.empdata.EmployeeData;

import lombok.Data;

@Entity
@Table(name = "tbl_emp_vaccination_status ")
@Data
public class EmployeeVaccinationStatus {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "v_status", nullable = false)
	private Boolean vaccinationStatus;
	
	@Column(name = "self_or_family")
	private String selfOrFamily;
	
	@Column(name = "tot_num_of_family_members")
	private Integer totNumOfFamilyMembers;
	
	@Column(name = "tot_num_of_family_members_vaccinated")
	private Integer totNumOfFamilyMembersVaccinated;
	
	@OneToOne
	@JoinColumn(name = "emp_data_id")
	private EmployeeData data;
	
	@OneToMany(mappedBy = "evs", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<EmployeCovidDoseAdministration> doseAdministrationlist;
	

}
