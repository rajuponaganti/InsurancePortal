package com.aizant.ip.empdata.covid;

import java.util.Date;
import java.util.Map;

import com.aizant.ip.util.IPUtil;

public class EmployeeCovidValidator {
	public String validate(EmployeeCovidDataDTO eCoviData) {
		String msg =null;
		if(eCoviData.getVaccinationStatus()!=null && eCoviData.getVaccinationStatus()) {
			//Total Family Members Must Be Greater Than or Equal to Total Family Members Vaccinated
			if(eCoviData.getSelfOrFamily()!=null && eCoviData.getSelfOrFamily().equalsIgnoreCase("FAMILY")) {
				if(eCoviData.getTotNumOfFamilyMembers()!=null && eCoviData.getTotNumOfFamilyMembersVaccinated()!=null && !(eCoviData.getTotNumOfFamilyMembers()>=eCoviData.getTotNumOfFamilyMembersVaccinated())) {
					return  "Total Family Members Must Be Greater Than or Equal to Total Family Members Vaccinated";
				}
				for (int i = 1; i <= eCoviData.getTotNumOfFamilyMembersVaccinated(); i++) {
					String msg1=  getValidationUtil(eCoviData, i);
					if(msg1!=null) return msg1;
				}
				
			}
			if(eCoviData.getSelfOrFamily()!=null && eCoviData.getSelfOrFamily().equalsIgnoreCase("SELF")) {
				return getValidationUtil(eCoviData, 1);
			}
			
		} 
		
		
		return msg;
	}
	
	String getValidationUtil(EmployeeCovidDataDTO eCoviData, Integer i) {
		Date firstVaccinationDate = getFirstVaccinationDate(eCoviData.getFirstDoseDates(), i);
		Date seconddoseDates = getSeconddoseDates(eCoviData.getSeconddoseDates(), i);
		Date boosterDoseDates = getBoosterDoseDates(eCoviData.getBoosterDoseDates(), i);
		String msg1 = DateValidations(firstVaccinationDate, seconddoseDates, boosterDoseDates);
		if (msg1 != null)
			return msg1;
		
		String firstVacRefNum = getFirstVaccinationReferenceNumbers(eCoviData.getFirstVaccinationReferenceNumbers(), i);
		firstVacRefNum = (firstVacRefNum != null) ? firstVacRefNum.trim() : null;
		
		String secondVacRefNum = getSecondVaccinationReferenceNumbers(eCoviData.getSecondVaccinationReferenceNumbers(), i);
		secondVacRefNum = (secondVacRefNum != null) ? secondVacRefNum.trim() : null;
		
		String boosterVacRefNum = getBoosterVaccinationReferenceNumbers(eCoviData.getBoosterVaccinationReferenceNumbers(), i);
		boosterVacRefNum = (boosterVacRefNum != null) ? boosterVacRefNum.trim() : null;
		
		if(secondVacRefNum!=null  && seconddoseDates==null   ) return "Enter Second Dose Vaccination Date";
		if(boosterVacRefNum!=null && boosterDoseDates==null) return "Enter Second Dose Vaccination Date";
		
		if (firstVacRefNum == null || firstVacRefNum == "")
			return "Enter First Dose Vaccination Ref Id";
		if (seconddoseDates != null && secondVacRefNum == null || secondVacRefNum == "")
			return "Enter Second Dose Vaccination Ref Id";
		if (boosterDoseDates != null && boosterVacRefNum == null || boosterVacRefNum == "")
			return "Enter Booster  Dose Vaccination Ref Id";
		String name = getFamilyMemberNames(eCoviData.getFamilyMemberNames(), i);
		name = (name != null) ? name.trim() : null;
		if(i==1 ) {
			if(name==null || name=="") {
				return "Enter Name "+i;	
			}
				
		}
		else {
			if(name == null  || name=="" && firstVacRefNum!=null && firstVaccinationDate!=null ) {
				return "Enter Name "+i;	
			}
		}
		String vaccinatedWith = getVaccinatedWiths(eCoviData.getVaccinatedWiths(), i);
		vaccinatedWith = (vaccinatedWith != null) ? vaccinatedWith.trim() : null;
		if(vaccinatedWith==null  ||  vaccinatedWith=="" && firstVacRefNum!=null && firstVaccinationDate!=null) {
			return "Enter Vaccinated With";
		}
		return null;
	}
	
	String DateValidations(Date firstVaccinationDate, Date seconddoseDate,  Date boosterDoseDate){
		if(firstVaccinationDate==null ) return "First Dose  Vaccination Date Could't Be Null";
		if(boosterDoseDate!=null && seconddoseDate==null) return "Second Dose  Vaccination Date Could't Be Null";
		if(firstVaccinationDate!=null && seconddoseDate!=null ) {
			if(seconddoseDate.after(firstVaccinationDate)) {
				if(seconddoseDate!=null && boosterDoseDate!=null && !(boosterDoseDate.after(seconddoseDate))) 
					return "Booter Dose Vaccination Date Should Be Greater Than The Second Dose Vaccination Date";
			}
			else return "Second Dose Vaccination Date  Should Be Greater Than The First Dose Vaccination Date";
		}
		return null;
	
	}

	Date getFirstVaccinationDate(Map<String, Object> firstDoseDatesMap, Integer i) {
		Date date  = null;
		if(firstDoseDatesMap!=null) {
			String value = (String)  firstDoseDatesMap.get("firstDose"+i);
			if(value!=null) {
				date = IPUtil.getutilDate(value);
				
			}
			
		}
		return date;
		
		
		
	}
	Date getSeconddoseDates( Map<String, Object> seconddoseDates, Integer i) {
		Date date  = null;
		if(seconddoseDates!=null) {
			String value = (String) seconddoseDates.get("seconddoses"+i);
			if(value!=null) {
				date = IPUtil.getutilDate(value);
			}
			
		}
		return date;
		
	}
	Date getBoosterDoseDates(Map<String, Object> boosterDoseDates, Integer i) {
		Date date  = null;
		if(boosterDoseDates!=null) {
			String value = (String) boosterDoseDates.get("boosterDose"+i);
			if(value!=null) {
				date = IPUtil.getutilDate(value);
			}
			
		}
		return date;
		
	}
	
	
	
	String getFirstVaccinationReferenceNumbers(Map<String, Object> firstVaccinationReferenceNumbers, Integer i) {
		String ref1 =null;
		if(firstVaccinationReferenceNumbers!=null) {
			ref1 = (String) firstVaccinationReferenceNumbers.get("firstVaccinationId"+i);
		}
		return ref1;  
		
	}
	
	String getSecondVaccinationReferenceNumbers(Map<String, Object> secondVaccinationReferenceNumbers, Integer i) {
		String ref2 =null;
		if(secondVaccinationReferenceNumbers!=null) {
			ref2 = (String) secondVaccinationReferenceNumbers.get("secondVaccinationIds"+i);
		}
		return ref2;  
		
	}
	
	String getBoosterVaccinationReferenceNumbers(Map<String, Object> boosterVaccinationReferenceNumbers, Integer i) {
		String ref3 =null;
		if(boosterVaccinationReferenceNumbers!=null) {
			ref3 = (String) boosterVaccinationReferenceNumbers.get("boosterVaccinationIds"+i);
		}
		return ref3;  
		
	}
	
	String getFamilyMemberNames( Map<String, Object> familyMemberNames, Integer i) {
		String name = null;
		if(familyMemberNames!=null) {
			name = (String) familyMemberNames.get("name"+i);
		}
		return name; 
		
	}
	String getVaccinatedWiths(Map<String, Object> vaccinatedWiths, Integer i) {
		String VaccinatedWith = null;
		if(vaccinatedWiths!=null) VaccinatedWith = (String) vaccinatedWiths.get("vaccinatedWith"+i);
		return VaccinatedWith;
		
	}
}
