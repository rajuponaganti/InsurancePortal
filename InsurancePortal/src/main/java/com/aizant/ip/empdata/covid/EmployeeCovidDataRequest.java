package com.aizant.ip.empdata.covid;

import com.aizant.ip.empdata.EmployeeData;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class EmployeeCovidDataRequest {

	private EmployeeData employeeData;
	private EmployeeCovidDataDTO employeeCovidData;
	private Boolean isExistedEmployee;
	private Boolean isCovidRegistered;
}
