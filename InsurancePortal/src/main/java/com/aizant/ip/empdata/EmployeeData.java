package com.aizant.ip.empdata;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tbl_emp_data")
@Setter
@Getter
public class EmployeeData {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "emp_code", nullable = false, unique = true)
	private Integer empCode;
	
	@Column(name = "emp_name", nullable = false)
	private String empName;
	
	@Column(name = "emp_department", nullable = false)
	private String empDepartment;
	
	
	@Column(name = "designation", nullable = false)
	private String designation;
	
	@Column(name = "emp_pan_number", nullable = false)
	private String  employeePanNumber;
	
	@Column(name = "emp_aadhar_number", nullable = false)
	private Long  empAadharNumber;
	
	@Column(name = "highest_qualification", nullable = true)
	private String  highestQualification;
	
	@Column(name = "personal_mobile_number", nullable = false)
	private Long  personalMobileNumber;
	
	@Column(name = "offical_mobile_number", nullable = true)
	private Long officalMobileNumber;
	
	@Column(name = "company_email_id", nullable = true)
	private String companyemailId;
	
	@Column(name = "emergency_contact_person", nullable = false)
	private String emergencyContactPersonName;
	
	@Column(name = "emergency_contact_person_num", nullable = false)
	private Long emergencyContactPersonNumber;
	
	@Column(name = "reporting_manager_emp_code", nullable = false)
	private Integer reportingManagerEmpCode;
	
	@Column(name = "reporting_manager_name", nullable = false)
	private String reportingManagerName;
	
	@Column(name = "reporting_manager_department", nullable = false)
	private String reportingManagerDepartment;
	
	@Column(name = "project_working_on", nullable = true)
	private String projectWorkingOn;
	
	@Column(name = "blood_group", nullable = false)
	private String bloodGroup;
	
	private Boolean status;
	
	
}
