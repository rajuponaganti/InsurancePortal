package com.aizant.ip.empdata;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/empData")
public class EmployeeDataController {

	@Autowired
	private IEmployeeDataService employeeDataService;
	
	@PostMapping
	public ResponseEntity<?> saveEmployeeData(@RequestBody EmployeeData employeeData){
		return ResponseEntity.ok(employeeDataService.saveEmployeeData(employeeData));
		
	}
	
	/*
	 * @PutMapping public ResponseEntity<?> updateEmployeeData(@RequestBody
	 * Map<String , Object> employeeData){ return
	 * ResponseEntity.ok(employeeDataService.updateEmployeeData(employeeData));
	 * 
	 * }
	 */
	
	@GetMapping
	public ResponseEntity<?> getEmployeesData(){
		return ResponseEntity.ok(employeeDataService.getEmployeesData());
		
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> getEmployeeData(@PathVariable Long id){
		return ResponseEntity.ok(employeeDataService.getEmployeeData(id));
		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> InactivateEmployeeData(@PathVariable Long id){
		return ResponseEntity.ok(employeeDataService.InactivateEmployeeData(id));
		
	}
	
	@GetMapping("/eCode")
	public ResponseEntity<?> isEmployeeExistedEmpCode(@RequestParam(name = "empCode", required = true ) Integer empCode){
		return ResponseEntity.ok(employeeDataService.isEmployeeExistedEmpCode(empCode));
		
	}

	// get employee details uisng empCode for onblur call
	// http://localhost:8080/empCovid/geteDetails?empCode=1234
	@GetMapping("/geteDetails")
	public ResponseEntity<?> getEmployeeDetails(@RequestParam String empCode) {
		return employeeDataService.getEmployeesDetails(empCode);

	}
	@GetMapping("/exportToXLS")
	public void exportToXLS(HttpServletResponse response){
		employeeDataService.exportToXLS(response);
		
	}
}
