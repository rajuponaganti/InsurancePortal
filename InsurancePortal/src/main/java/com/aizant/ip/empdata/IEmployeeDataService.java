package com.aizant.ip.empdata;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;


public interface IEmployeeDataService {

	ResponseEntity<?> saveEmployeeData(EmployeeData employeeData);

	/* ResponseEntity<?> updateEmployeeData(Map<String, Object> employeeData); */

	ResponseEntity<?> getEmployeesData();

	ResponseEntity<?> getEmployeeData(Long id);

	Object InactivateEmployeeData(Long id);

	ResponseEntity<?> isEmployeeExistedEmpCode(Integer empCode);

	ResponseEntity<?> getEmployeesDetails(String empCode);
	
	void exportToXLS(HttpServletResponse response);

}
