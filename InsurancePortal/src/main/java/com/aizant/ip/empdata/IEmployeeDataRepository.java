package com.aizant.ip.empdata;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface IEmployeeDataRepository extends JpaRepository<EmployeeData, Long>, JpaSpecificationExecutor<EmployeeData>{

}
