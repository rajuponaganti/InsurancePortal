package com.aizant.ip.empdata.covid;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;

public interface IEmployeeCovidService {

	ResponseEntity<?> saveEmployeeCovidData(EmployeeCovidDataRequest req);

	ResponseEntity<?> getEmployeeCovidData(Long id);

	ResponseEntity<?> getAllEmployeesCovidData();
	
	void getEmpCovidDetailsByIdUtils(EmployeeVaccinationStatus employeeVaccinationStatus, EmployeeCovidDataRequest req);

	ResponseEntity<?> updateEmployeeCovidData(EmployeeCovidDataRequest req);

	void exportToXLS(HttpServletResponse response);

}
