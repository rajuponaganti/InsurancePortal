package com.aizant.ip.empdata.covid;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "tbl_emp_covid_Doses_administration")
@Data
public class EmployeCovidDoseAdministration {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String vaccinationWith;

	private Date firstVaccinationDate;

	private String firstVaccinationRefNum;

	private Date secondVaccinationDate;

	private String secondVaccinationRefNum;
	
	private Date boosterVaccinationDate;

	private String boosterVaccinationRefNum;
	
	private String name;
	
	@Column(name = "is_employee" )
	private Boolean isEmployee;
	
	
	@JsonIgnore
	@ManyToOne()
	@JoinColumn(name = "evs_id", nullable = false, referencedColumnName = "id")
	private EmployeeVaccinationStatus evs;
}
