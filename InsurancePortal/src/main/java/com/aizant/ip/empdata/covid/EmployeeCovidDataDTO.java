package com.aizant.ip.empdata.covid;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;
@Setter
@Getter
public class EmployeeCovidDataDTO {
	
	private Long id;
	
	private String selfOrFamily;
	
	private Integer dosesCompleted;
	
	private Integer totNumOfFamilyMembers;
	private Integer totNumOfFamilyMembersVaccinated;
	
	private Boolean vaccinationStatus;
	
	private Map<String, Object> familyMemberNames;
	private Map<String, Object> vaccinatedWiths;
	private Map<String, Object> firstDoseDates;
	private Map<String, Object> firstVaccinationReferenceNumbers;
	private Map<String, Object> seconddoseDates;
	private Map<String, Object> secondVaccinationReferenceNumbers;
	private Map<String, Object> boosterDoseDates;
	private Map<String, Object> boosterVaccinationReferenceNumbers;
	private Map<String, Object> familyMemberIDs;
}
