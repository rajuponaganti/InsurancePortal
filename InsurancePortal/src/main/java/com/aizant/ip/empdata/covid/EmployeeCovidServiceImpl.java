package com.aizant.ip.empdata.covid;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.aizant.ip.IPResponce;
import com.aizant.ip.empdata.EmployeeData;
import com.aizant.ip.util.IPException;
import com.aizant.ip.util.IPUtil;

@Service
@Transactional
public class EmployeeCovidServiceImpl implements IEmployeeCovidService{
	private static final Logger logger = LoggerFactory.getLogger(EmployeeCovidServiceImpl.class);
	
	@Autowired
	private IEmployeeVaccinationStatusRepository evsRepo;
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ResponseEntity<?> saveEmployeeCovidData(EmployeeCovidDataRequest req) {
		logger.info("In EmployeeCovidServiceImpl : saveEmployeeCovidData () ");
		ResponseEntity<?> responseEntity = null;
		try {
			responseEntity =  ResponseEntity.ok(new IPResponce(Objects.nonNull(evsRepo.save(processSaveReq(req)))) );
		} catch (IPException e) {
			logger.error("In EmployeeCovidServiceImpl : saveEmployeeCovidData () message", e);
			responseEntity =  ResponseEntity.ok(new IPResponce(e.getMessage()) );
		}
		catch (Exception e) {
			logger.error("In EmployeeCovidServiceImpl : saveEmployeeCovidData () StackTrace", e);
			responseEntity =  ResponseEntity.ok(new IPResponce(e.getMessage()) );
		}
		return responseEntity;
	}
	
	EmployeeVaccinationStatus processSaveReq(EmployeeCovidDataRequest req) throws IPException {
		EmployeeVaccinationStatus evs = null;
		if (req != null) {
			List<EmployeCovidDoseAdministration> list = new ArrayList<EmployeCovidDoseAdministration>();
			EmployeeCovidDataDTO eCoviData = req.getEmployeeCovidData();
			if (eCoviData != null) {
				String validate = new EmployeeCovidValidator().validate(eCoviData);
				if(validate!=null) 
					throw new IPException(validate);
				else {
					if (eCoviData.getId() != null) {
						Optional<EmployeeVaccinationStatus> optional = evsRepo.findById(eCoviData.getId());
						evs = (optional.isPresent()) ? optional.get() : null;
						//validations
						//1.From Family to Self not Possible
						if(evs!=null) {
							if(evs.getVaccinationStatus()!=null && evs.getVaccinationStatus()) {
								String savedselfOrFamily = evs.getSelfOrFamily();
								String reqSelfOrFamily = eCoviData.getSelfOrFamily();
								if(savedselfOrFamily!=null && reqSelfOrFamily!=null && !savedselfOrFamily.equalsIgnoreCase(reqSelfOrFamily)) {
									if(savedselfOrFamily.equalsIgnoreCase("FAMILY") && reqSelfOrFamily.equalsIgnoreCase("SELF")) 
									throw new IPException("Update From Family to Self Not Possible");	
									
								}
							}
							
							
						}
					} else {
						evs = new EmployeeVaccinationStatus();
					}
					
					evs.setData(req.getEmployeeData());
					evs.setSelfOrFamily(eCoviData.getSelfOrFamily());
					evs.setVaccinationStatus(eCoviData.getVaccinationStatus());
					evs.setTotNumOfFamilyMembers(eCoviData.getTotNumOfFamilyMembers());
					evs.setTotNumOfFamilyMembersVaccinated(eCoviData.getTotNumOfFamilyMembersVaccinated());
					if (evs.getVaccinationStatus() != null && evs.getVaccinationStatus()) {
						if (evs.getSelfOrFamily() != null && evs.getSelfOrFamily().equalsIgnoreCase("FAMILY")) {
							for (int i = 1; i <= eCoviData.getTotNumOfFamilyMembersVaccinated(); i++) {
								EmployeCovidDoseAdministration empCovidAdmin = new EmployeCovidDoseAdministration();
								EmployeeData employeeData = req.getEmployeeData();
								if(employeeData!=null) {
									String empName = employeeData.getEmpName();
									String familyMemberName = getFamilyMemberName(eCoviData.getFamilyMemberNames(), i);
									if(empName!=null && familyMemberName!=null && empName.equalsIgnoreCase(familyMemberName)) 
										empCovidAdmin.setIsEmployee(true);
									
								}
								getUtil1(empCovidAdmin, eCoviData, i, list, evs);
							}
								

						}
						if (evs.getSelfOrFamily() != null && evs.getSelfOrFamily().equalsIgnoreCase("SELF")) {
							EmployeCovidDoseAdministration ecda = new EmployeCovidDoseAdministration();
							ecda.setIsEmployee(true);
							if(req.getEmployeeData()!=null) ecda.setName(req.getEmployeeData().getEmpName());
							getUtil1(ecda, eCoviData, 1, list, evs);
						}
							
						evs.setDoseAdministrationlist(list);
					}

				}
				}
				

		}
		return evs;
	}
	String getFamilyMemberName(Map<String, Object> familyMemberNames, Integer i) {
		String name = null;
		if(familyMemberNames!=null) name =  (String) familyMemberNames.get("name"+i);
		return name;
		
	}
	
	public void getUtil1(EmployeCovidDoseAdministration ecda, EmployeeCovidDataDTO eCoviData, Integer i, List<EmployeCovidDoseAdministration> list, EmployeeVaccinationStatus evs) {
		ecda.setEvs(evs);
		if(eCoviData.getFamilyMemberIDs() !=null) getFamilyMemberIDs(ecda, eCoviData.getFamilyMemberIDs(), i);
		getFamilyMemberNames(ecda, eCoviData.getFamilyMemberNames(), i);
		getVaccinatedWiths(ecda, eCoviData.getVaccinatedWiths(), i);
		getFirstVaccinationDate(ecda, eCoviData.getFirstDoseDates(), i);
		getFirstVaccinationReferenceNumbers(ecda, eCoviData.getFirstVaccinationReferenceNumbers(), i);
		getSeconddoseDates(ecda, eCoviData.getSeconddoseDates(), i);
		getSecondVaccinationReferenceNumbers(ecda, eCoviData.getSecondVaccinationReferenceNumbers(), i);
		getBoosterDoseDates(ecda, eCoviData.getBoosterDoseDates(), i);
		getBoosterVaccinationReferenceNumbers(ecda, eCoviData.getBoosterVaccinationReferenceNumbers(), i);
		list.add(ecda);
	}
	

	// for ids
	void getFamilyMemberIDs(EmployeCovidDoseAdministration ecda, Map<String, Object> familyMemberIDs, Integer i) {
		if (familyMemberIDs != null) {
			if (familyMemberIDs.get("vaccineID" + i) != null) {
				ecda.setId(Long.valueOf(familyMemberIDs.get("vaccineID" + i).toString()));
			}
		}
		
	}
	
	void getFamilyMemberNames(EmployeCovidDoseAdministration ecda, Map<String, Object> familyMemberNames, Integer i) {
		if(familyMemberNames!=null)  ecda.setName((String) familyMemberNames.get("name"+i));
		
	}
	void getVaccinatedWiths(EmployeCovidDoseAdministration ecda, Map<String, Object> vaccinatedWiths, Integer i) {
		if(vaccinatedWiths!=null) ecda.setVaccinationWith((String) vaccinatedWiths.get("vaccinatedWith"+i));
		
	}
	void getFirstVaccinationDate(EmployeCovidDoseAdministration ecda, Map<String, Object> firstDoseDatesMap, Integer i) {
		if(firstDoseDatesMap!=null) {
			String value = (String)  firstDoseDatesMap.get("firstDose"+i);
			if(value!=null) {
				java.util.Date getutilDate = IPUtil.getutilDate(value);
				ecda.setFirstVaccinationDate(new Date(getutilDate.getTime()));
			}
			
		}
		
		
		
	}
	void getFirstVaccinationReferenceNumbers(EmployeCovidDoseAdministration ecda, Map<String, Object> firstVaccinationReferenceNumbers, Integer i) {
		if(firstVaccinationReferenceNumbers!=null)  ecda.setFirstVaccinationRefNum((String) firstVaccinationReferenceNumbers.get("firstVaccinationId"+i));
		
	}
	void getSeconddoseDates(EmployeCovidDoseAdministration ecda, Map<String, Object> seconddoseDates, Integer i) {
		if(seconddoseDates!=null) {
			String value = (String) seconddoseDates.get("seconddoses"+i);
			if(value!=null) {
				java.util.Date getutilDate = IPUtil.getutilDate(value);
				ecda.setSecondVaccinationDate(new Date(getutilDate.getTime()));
			}
			
		}
		
	}
	void getSecondVaccinationReferenceNumbers(EmployeCovidDoseAdministration ecda, Map<String, Object> secondVaccinationReferenceNumbers, Integer i) {
		if(secondVaccinationReferenceNumbers!=null)   ecda.setSecondVaccinationRefNum((String) secondVaccinationReferenceNumbers.get("secondVaccinationIds"+i));
		
	}
	void getBoosterDoseDates(EmployeCovidDoseAdministration ecda, Map<String, Object> boosterDoseDates, Integer i) {
		if(boosterDoseDates!=null) {
			String value = (String) boosterDoseDates.get("boosterDose"+i);
			if(value!=null) {
				java.util.Date getutilDate = IPUtil.getutilDate(value);
				ecda.setBoosterVaccinationDate(new Date(getutilDate.getTime()));
			}
			
		}
		
	}
	void getBoosterVaccinationReferenceNumbers(EmployeCovidDoseAdministration ecda, Map<String, Object> boosterVaccinationReferenceNumbers, Integer i) {
		if(boosterVaccinationReferenceNumbers!=null)  ecda.setBoosterVaccinationRefNum((String) boosterVaccinationReferenceNumbers.get("boosterVaccinationIds"+i));
		
	}
	

	@Override
	public ResponseEntity<?> getEmployeeCovidData(Long id) {
		logger.info("In EmployeeCovidServiceImpl : getEmployeeCovidData () "+id);
		ResponseEntity<?> responseEntity = null;
		try {
			EmployeeCovidDataRequest req= new EmployeeCovidDataRequest ();
			Optional<EmployeeVaccinationStatus> optional = evsRepo.findById(id);
			if(optional.isPresent()) {
				EmployeeVaccinationStatus employeeVaccinationStatus = optional.get();
				if(employeeVaccinationStatus.getVaccinationStatus()!=null) {
					if(employeeVaccinationStatus.getVaccinationStatus()) {
						getEmpCovidDetailsByIdUtils(employeeVaccinationStatus, req);
						req.setEmployeeData(employeeVaccinationStatus.getData());
						responseEntity = ResponseEntity.ok(req);
					}
					else {
						 req.setIsCovidRegistered(false);
						 responseEntity = ResponseEntity.ok(employeeVaccinationStatus);
					}
				}
				
			}
		} catch (Exception e) {
			logger.error("In EmployeeCovidServiceImpl : getEmployeeCovidData () ", e);
		}
		
		return responseEntity;
	}
	
	@Override
	public void getEmpCovidDetailsByIdUtils(EmployeeVaccinationStatus employeeVaccinationStatus, EmployeeCovidDataRequest req) {
		logger.info("In EmployeeCovidServiceImpl : getEmpCovidDetailsByIdUtils () ");
		try {
			EmployeeData employeeData = employeeVaccinationStatus.getData();
			if(employeeVaccinationStatus.getVaccinationStatus()!=null) {
				if(employeeVaccinationStatus.getVaccinationStatus()) {
					req.setEmployeeData(employeeData);
					List<EmployeCovidDoseAdministration> administrationlist = employeeVaccinationStatus.getDoseAdministrationlist();
					EmployeeCovidDataDTO dto= new EmployeeCovidDataDTO();
					dto.setId(employeeVaccinationStatus.getId());
					Map<String, Object> familyMemberNames = new LinkedHashMap<>();
					Map<String, Object> familyMemberIDs = new LinkedHashMap<>();
					Map<String, Object> vaccinatedWiths = new LinkedHashMap<>();
					Map<String, Object> firstDoseDates = new LinkedHashMap<>();
					Map<String, Object> firstVaccinationReferenceNumbers = new LinkedHashMap<>();
					Map<String, Object> seconddoseDates = new LinkedHashMap<>();
					Map<String, Object> secondVaccinationReferenceNumbers = new LinkedHashMap<>();
					Map<String, Object> boosterDoseDates = new LinkedHashMap<>();
					Map<String, Object> boosterVaccinationReferenceNumbers = new LinkedHashMap<>();
					if(employeeVaccinationStatus.getSelfOrFamily()!=null && employeeVaccinationStatus.getSelfOrFamily().equalsIgnoreCase("FAMILY")) {
						for (int i = 0; i < administrationlist.size(); i++) {
							int j= i+1;
							EmployeCovidDoseAdministration employeCovidDoseAdministration = administrationlist.get(i);
							if(employeCovidDoseAdministration.getId() != null) familyMemberIDs.put("vaccineID" + j, employeCovidDoseAdministration.getId());
							if( employeCovidDoseAdministration.getName() !=null) familyMemberNames.put("name"+j, employeCovidDoseAdministration.getName());
							if(employeCovidDoseAdministration.getVaccinationWith() !=null) vaccinatedWiths.put("vaccinatedWith"+j, employeCovidDoseAdministration.getVaccinationWith());
							Date firstVaccinationDate = employeCovidDoseAdministration.getFirstVaccinationDate();
							if(firstVaccinationDate !=null) firstDoseDates.put("firstDose"+j, new java.util.Date(firstVaccinationDate.getTime()));
							if(employeCovidDoseAdministration.getFirstVaccinationRefNum() !=null) firstVaccinationReferenceNumbers.put("firstVaccinationId"+j, employeCovidDoseAdministration.getFirstVaccinationRefNum());
							Date secondVaccinationDate = employeCovidDoseAdministration.getSecondVaccinationDate();
							if(secondVaccinationDate !=null) seconddoseDates.put("seconddoses"+j, new java.util.Date(secondVaccinationDate.getTime()));
							if(employeCovidDoseAdministration.getSecondVaccinationRefNum() !=null) secondVaccinationReferenceNumbers.put("secondVaccinationIds"+j, employeCovidDoseAdministration.getSecondVaccinationRefNum());
							Date boosterVaccinationDate = employeCovidDoseAdministration.getBoosterVaccinationDate();
							if(boosterVaccinationDate !=null) boosterDoseDates.put("boosterDose"+j, new java.util.Date(boosterVaccinationDate.getTime()) );
							if(employeCovidDoseAdministration.getBoosterVaccinationRefNum() !=null) boosterVaccinationReferenceNumbers.put("boosterVaccinationIds"+j, employeCovidDoseAdministration.getBoosterVaccinationRefNum());
						}
						
						
					}
					if(employeeVaccinationStatus.getSelfOrFamily()!=null && employeeVaccinationStatus.getSelfOrFamily().equalsIgnoreCase("SELF")) {
						EmployeCovidDoseAdministration employeCovidDoseAdministration = administrationlist.get(0);
						if(employeCovidDoseAdministration.getId() != null) familyMemberIDs.put("vaccineID" + 1, employeCovidDoseAdministration.getId());
						if( employeCovidDoseAdministration.getName() !=null) familyMemberNames.put("name"+1, employeCovidDoseAdministration.getName());
						if(employeCovidDoseAdministration.getVaccinationWith() !=null) vaccinatedWiths.put("vaccinatedWith"+1, employeCovidDoseAdministration.getVaccinationWith());
						Date firstVaccinationDate = employeCovidDoseAdministration.getFirstVaccinationDate();
						if(firstVaccinationDate !=null) firstDoseDates.put("firstDose"+1, new java.util.Date(firstVaccinationDate.getTime()));
						if(employeCovidDoseAdministration.getFirstVaccinationRefNum() !=null) firstVaccinationReferenceNumbers.put("firstVaccinationId"+1, employeCovidDoseAdministration.getFirstVaccinationRefNum());
						Date secondVaccinationDate = employeCovidDoseAdministration.getSecondVaccinationDate();
						if(secondVaccinationDate !=null) seconddoseDates.put("seconddoses"+1, new java.util.Date(secondVaccinationDate.getTime()));
						if(employeCovidDoseAdministration.getSecondVaccinationRefNum() !=null) secondVaccinationReferenceNumbers.put("secondVaccinationIds"+1, employeCovidDoseAdministration.getSecondVaccinationRefNum());
						Date boosterVaccinationDate = employeCovidDoseAdministration.getBoosterVaccinationDate();
						if(boosterVaccinationDate !=null) boosterDoseDates.put("boosterDose"+1, new java.util.Date(boosterVaccinationDate.getTime()));
						if(employeCovidDoseAdministration.getBoosterVaccinationRefNum() !=null) boosterVaccinationReferenceNumbers.put("boosterVaccinationIds"+1, employeCovidDoseAdministration.getBoosterVaccinationRefNum());
					}
					dto.setTotNumOfFamilyMembers(employeeVaccinationStatus.getTotNumOfFamilyMembers());
					dto.setTotNumOfFamilyMembersVaccinated(employeeVaccinationStatus.getTotNumOfFamilyMembersVaccinated());
					dto.setVaccinationStatus(employeeVaccinationStatus.getVaccinationStatus());
					dto.setSelfOrFamily(employeeVaccinationStatus.getSelfOrFamily());
					dto.setFamilyMemberNames(familyMemberNames);
					dto.setVaccinatedWiths(vaccinatedWiths);
					dto.setVaccinationStatus(employeeVaccinationStatus.getVaccinationStatus());
					dto.setFirstDoseDates(firstDoseDates);
					dto.setFirstVaccinationReferenceNumbers(firstVaccinationReferenceNumbers);
					dto.setSeconddoseDates(seconddoseDates);
					dto.setSecondVaccinationReferenceNumbers(secondVaccinationReferenceNumbers);
					dto.setBoosterDoseDates(boosterDoseDates);
					dto.setBoosterVaccinationReferenceNumbers(boosterVaccinationReferenceNumbers);
					dto.setFamilyMemberIDs(familyMemberIDs);
					req.setEmployeeCovidData(dto);
				}
				else {
					EmployeeCovidDataDTO dto= new EmployeeCovidDataDTO();
					dto.setVaccinationStatus(employeeVaccinationStatus.getVaccinationStatus());
					dto.setId(employeeVaccinationStatus.getId());
					req.setEmployeeCovidData(dto);
				}
			}
		} catch (Exception e) {
			logger.error("In EmployeeCovidServiceImpl : getEmpCovidDetailsByIdUtils () ", e);
		}

	}

	@Override
	public ResponseEntity<?> getAllEmployeesCovidData() {
		logger.info("In EmployeeCovidServiceImpl : getAllEmployeesCovidData () ");
		List<EmployeeVaccinationStatus> list = null; 
		try {
			list = evsRepo.findAll();
		} catch (Exception e) {
			logger.error("In EmployeeCovidServiceImpl : getAllEmployeesCovidData () ", e);
		}
		return ResponseEntity.ok(list);
	}

	@Override
	public ResponseEntity<?> updateEmployeeCovidData(EmployeeCovidDataRequest req) {
		logger.info("In EmployeeCovidServiceImpl : updateEmployeeCovidData () ");
		ResponseEntity<?> responseEntity = null;
		try {
			responseEntity =  ResponseEntity.ok(new IPResponce(Objects.nonNull(evsRepo.save(updateCovidReq(req)))) );
		} catch (IPException e) {
			logger.error("In EmployeeCovidServiceImpl : updateEmployeeCovidData () ", e);
			responseEntity =  ResponseEntity.ok(new IPResponce(e.getMessage()) );
		}
		return responseEntity;
		
	}
	
	
	public EmployeeVaccinationStatus updateCovidReq(EmployeeCovidDataRequest req) throws IPException {
		logger.info("In EmployeeCovidServiceImpl : updateCovidReq () ");
		EmployeeVaccinationStatus evs = null;
		try {
			evs = null;
			if (req != null) {
				EmployeeCovidDataDTO eCoviData = req.getEmployeeCovidData();
				if (eCoviData != null) {
					String validate = new EmployeeCovidValidator().validate(eCoviData);
					if(validate!=null) 
						throw new IPException(validate);
					if (eCoviData.getId() != null && eCoviData.getId() > 0) {
						Optional<EmployeeVaccinationStatus> optional = evsRepo.findById(eCoviData.getId());
						evs = (optional.isPresent()) ? optional.get() : null;
					} else
						evs = new EmployeeVaccinationStatus();
					evs.setData(req.getEmployeeData());
					evs.setSelfOrFamily(eCoviData.getSelfOrFamily());
					evs.setVaccinationStatus(eCoviData.getVaccinationStatus());
					evs.setTotNumOfFamilyMembers(eCoviData.getTotNumOfFamilyMembers());
					evs.setTotNumOfFamilyMembersVaccinated(eCoviData.getTotNumOfFamilyMembersVaccinated());
					List<EmployeCovidDoseAdministration> list = new ArrayList<EmployeCovidDoseAdministration>();
					if (eCoviData.getVaccinationStatus() != null) {
						if (eCoviData.getVaccinationStatus()) {
							if (eCoviData.getSelfOrFamily() != null
									&& eCoviData.getSelfOrFamily().equalsIgnoreCase("FAMILY")) {
								for (int i = 1; i <= eCoviData.getTotNumOfFamilyMembersVaccinated(); i++)
									getUtil1(new EmployeCovidDoseAdministration(), eCoviData, i, list, evs);

							}
							if (eCoviData.getSelfOrFamily() != null && eCoviData.getSelfOrFamily().equalsIgnoreCase("SELF"))
								getUtil1(new EmployeCovidDoseAdministration(), eCoviData, 1, list, evs);
							evs.setDoseAdministrationlist(list);
						}

					}

				}

			}
		} catch (IPException e) {
			logger.error("In EmployeeCovidServiceImpl : updateCovidReq () ", e);
		}
		return evs;

	}

	@Override
	public void exportToXLS(HttpServletResponse response) {
		logger.info("In EmployeeCovidServiceImpl : exportToXLS () ");
		try {
			List<EmployeeVaccinationStatus> list = evsRepo.findAll();
			List<EmployeeCovidExportDTO> exportDTOList = new ArrayList<EmployeeCovidExportDTO>();
			for (EmployeeVaccinationStatus evs : list) {
				EmployeeCovidExportDTO exportDTO = new EmployeeCovidExportDTO();
				EmployeeData data = evs.getData();
				if (evs.getVaccinationStatus() != null) {
					if (evs.getVaccinationStatus()) {
						EmployeCovidDoseAdministration ecda = null;
						List<EmployeCovidDoseAdministration> doseAdministrationlist = evs.getDoseAdministrationlist();
						for (EmployeCovidDoseAdministration ad : doseAdministrationlist) {
							if (ad != null && ad.getIsEmployee() != null && ad.getIsEmployee()) {
								ecda = ad;
							}
						}
						if (ecda != null) {
							if (evs.getSelfOrFamily() != null && evs.getSelfOrFamily().equalsIgnoreCase("FAMILY")) {
								exportDTO.setVaccinationStatusFamily("Yes");
								exportDTO.setVaccinationStatusSelf("No");
							}
							if (evs.getSelfOrFamily() != null && evs.getSelfOrFamily().equalsIgnoreCase("SELF")) {
								exportDTO.setVaccinationStatusSelf("Yes");
								exportDTO.setVaccinationStatusFamily("No");
							}
							exportDTO.setVaccinatedwith(ecda.getVaccinationWith());
							LocalDate vaccinationDate = null;
							String vaccinationRef = null;
							String doseAdministered = null;
							if (ecda.getFirstVaccinationDate() != null && ecda.getFirstVaccinationRefNum() != null) {
								vaccinationDate = Instant.ofEpochMilli(ecda.getFirstVaccinationDate().getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
								vaccinationRef = ecda.getFirstVaccinationRefNum();
								doseAdministered = "First Dose";
								if (ecda.getSecondVaccinationDate() != null && ecda.getSecondVaccinationRefNum() != null) {
									vaccinationDate = Instant.ofEpochMilli(ecda.getSecondVaccinationDate().getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
									vaccinationRef = ecda.getSecondVaccinationRefNum();
									doseAdministered = "Second Dose";
									if (ecda.getBoosterVaccinationDate() != null && ecda.getBoosterVaccinationRefNum() != null) {
										vaccinationDate = Instant.ofEpochMilli(ecda.getBoosterVaccinationDate().getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
										vaccinationRef = ecda.getBoosterVaccinationRefNum();
										doseAdministered = "Booster Dose";
									}
								}

							}
							if (doseAdministered != null)
								exportDTO.setDoseAdministered(doseAdministered);
							if (vaccinationDate != null && vaccinationRef != null) {
								exportDTO.setProofOfVaccination(vaccinationDate.toString().concat(" ").concat(vaccinationRef));
							}

							
						}
					} else {
						exportDTO.setVaccinationStatusSelf("No");
						exportDTO.setVaccinationStatusFamily("No");
					}
					exportDTO.setEmpCode(data.getEmpCode());
					exportDTO.setEmpName(data.getEmpName());
					exportDTO.setDepartment(data.getEmpDepartment());
				}
				exportDTOList.add(exportDTO);
			}
			String currentDateTime = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new java.util.Date());
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFCellStyle textStyle = getTextFormat(workbook);
			XSSFCellStyle numStyle = getNumberFormat(workbook);
			String[] aeColumns = { "Emp.ID", "Emp.Name", "Department", "Vaccination Status(Self)",
					"Vaccination Status(Family)", "Vaccinated with", "Dose administered", "Proof of vaccination" };
			response.setHeader("Content-Disposition",
					"attachment; filename=" + "Employee_covid_Details" + "_" + currentDateTime + ".xlsx");
			XSSFSheet aeSheet = getExcelSheet(workbook, aeColumns);
			int rowNum = 1;
			for (EmployeeCovidExportDTO dto : exportDTOList) {
				Row row = aeSheet.createRow(rowNum++);
				setCellValueAndFormat(row.createCell(0), numStyle, dto.getEmpCode());
				setCellValueAndFormat(row.createCell(1), textStyle, dto.getEmpName());
				setCellValueAndFormat(row.createCell(2), textStyle, dto.getDepartment());
				setCellValueAndFormat(row.createCell(3), textStyle, dto.getVaccinationStatusSelf());
				setCellValueAndFormat(row.createCell(4), textStyle, dto.getVaccinationStatusFamily());
				setCellValueAndFormat(row.createCell(5), textStyle, dto.getVaccinatedwith());
				setCellValueAndFormat(row.createCell(6), textStyle, dto.getDoseAdministered());

				setCellValueAndFormat(row.createCell(7), textStyle, dto.getProofOfVaccination());

			}
			closeOutStream(response, workbook);
		} catch (Exception e) {
			logger.error("In EmployeeDataServiceImpl : exportToXLS () ");
		}

	}
	private XSSFCellStyle getTextFormat(XSSFWorkbook workbook) {
		XSSFCellStyle textStyle = workbook.createCellStyle();
		textStyle.setDataFormat(workbook.createDataFormat().getFormat("@"));
		return textStyle;
	}
	private void setCellValueAndFormat(Cell cell, XSSFCellStyle style, String value) {
		if(value!=null)cell.setCellValue(value);
		else cell.setCellValue("");
		cell.setCellStyle(style);
	}
	private XSSFSheet getExcelSheet(XSSFWorkbook workbook, String[] columns) {
		XSSFSheet sheet = workbook.createSheet("Sheet1");
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 11);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		headerFont.setFontName("Calibri");
		Row headerRow = sheet.createRow(0);
		XSSFCellStyle style = workbook.createCellStyle();
		style.setFont(headerFont);
		for (int i = 0; i < columns.length; i++) setCellValueAndFormat(headerRow.createCell(i), style, columns[i]);
		return sheet;
	}
	private XSSFCellStyle getNumberFormat(XSSFWorkbook workbook) {
		XSSFCellStyle numberStyle = workbook.createCellStyle();
		numberStyle.setDataFormat(workbook.createDataFormat().getFormat("0"));
		return numberStyle;
	}
	private void setCellValueAndFormat(Cell cell, XSSFCellStyle style,  Number number) {
		if(number!=null) {
			if(number instanceof Integer) cell.setCellValue(number.intValue());
			else if(number instanceof Double) cell.setCellValue(number.doubleValue());
			else if(number instanceof Float) cell.setCellValue(number.floatValue());
			else if(number instanceof Byte) cell.setCellValue(number.byteValue());
			else if(number instanceof Short) cell.setCellValue(number.shortValue());
			else if(number instanceof Long) cell.setCellValue(number.longValue());	
		}
		else cell.setCellValue("");
		cell.setCellStyle(style);
	}
	private void closeOutStream(HttpServletResponse response, XSSFWorkbook workbook) {
		try {
			ServletOutputStream outputStream = response.getOutputStream();
			workbook.write(outputStream);
			outputStream.close();
			workbook.close();
		} catch (IOException e) {
		}
	}


}
