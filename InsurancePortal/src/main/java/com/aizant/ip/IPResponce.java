package com.aizant.ip;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class IPResponce {
	private Boolean flag;
	private String msg;

	public IPResponce() {
	}

	public IPResponce(Boolean flag) {
		super();
		this.flag = flag;
	}

	public IPResponce(String msg) {
		super();
		this.msg = msg;
	}

}
