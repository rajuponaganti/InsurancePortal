package com.aizant.ip.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class IPConstants {

	public final String INTERNAL_SERVER_ERROR="Internal Server Error";
}
