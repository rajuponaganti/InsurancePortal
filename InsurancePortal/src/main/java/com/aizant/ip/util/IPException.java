package com.aizant.ip.util;

public class IPException extends Exception{

	private static final long serialVersionUID = 3303442997095154712L;
	public IPException() {
	}
	public IPException(String message) {
		super(message);
	}

}
