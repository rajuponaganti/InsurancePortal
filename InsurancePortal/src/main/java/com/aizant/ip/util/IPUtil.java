package com.aizant.ip.util;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import lombok.experimental.UtilityClass;
@UtilityClass
public class IPUtil {
	public java.util.Date getutilDate(String source) {
		java.util.Date dateForCassandra=null;
		LocalDateTime plusDays=null;
		Date date =null;
		java.util.Date from =null;
		LocalDateTime ldt = LocalDateTime.parse(source, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"));
		if(ldt!=null) {
			ZoneId indianZone = ZoneId.of("Asia/Kolkata");
	        ZonedDateTime asiaZonedDateTime = ldt.atZone(indianZone);
	         dateForCassandra = java.util.Date.from( asiaZonedDateTime.toInstant() );
	         LocalDateTime localDateTime = dateForCassandra.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
	          plusDays = localDateTime.plusDays(1);
	           from = java.util.Date.from(plusDays.toLocalDate().atStartOfDay()
	        	      .atZone(ZoneId.systemDefault())
	        	      .toInstant());
		}
        if(dateForCassandra!=null) {
        	 date = new java.sql.Date(from.getTime());
        }
		return date;
	}
}
