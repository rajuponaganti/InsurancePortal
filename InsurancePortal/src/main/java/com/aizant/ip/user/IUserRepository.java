package com.aizant.ip.user;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRepository extends JpaRepository<User, Long> {
	User findByUserName(String userName);


}
