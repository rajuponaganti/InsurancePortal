package com.aizant.ip.user;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserResponse {
	private String username;
	private Collection<? extends GrantedAuthority> authorities;
	private String token;
	private Boolean isAuthenticatedUser;
	private String msg;
	public UserResponse() {
	}
	public UserResponse(String msg) {
		super();
		this.msg = msg;
	}
	
}
