package com.aizant.ip.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.aizant.ip.security.JwtAuthenticationRequest;
import com.aizant.ip.security.JwtAuthenticationResponse;
import com.aizant.ip.security.JwtTokenUtil;
import com.aizant.ip.security.JwtUser;

@RestController
public class AuthenticationController {
	
	private static final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);
	
	@Value("${jwt.header}")
	private String tokenHeader;
	
	

	@Value("${jwt.expiration}")
	private Long expiration;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private IUserService userSerivce;
	
	
	/*
	 * @RequestMapping(value = "/login", method = RequestMethod.POST) public
	 * ResponseEntity<?> createAuthenticationToken(@RequestBody
	 * JwtAuthenticationRequest authenticationRequest){ ResponseEntity
	 * responseEntity=null; UserResponse resp = new UserResponse(); try{ final
	 * Authentication authentication = authenticationManager.authenticate(new
	 * UsernamePasswordAuthenticationToken(authenticationRequest.getUserName(),
	 * authenticationRequest.getPassword())); if(authentication!=null &&
	 * authentication.isAuthenticated()) {
	 * SecurityContextHolder.getContext().setAuthentication(authentication); final
	 * UserDetails userDetails =
	 * userDetailsService.loadUserByUsername(authenticationRequest.getUserName());
	 * List<GrantedAuthority> authorities= (List<GrantedAuthority>)
	 * userDetails.getAuthorities(); resp.setAuthorities(authorities); if
	 * (authorities != null && authorities.size() > 0 ) { GrantedAuthority
	 * grantedAuthority = authorities.get(0); if (grantedAuthority != null &&
	 * grantedAuthority.getAuthority() != null &&
	 * grantedAuthority.getAuthority().equalsIgnoreCase("ROLE_USER") ) { LocalDate
	 * localDate = LocalDate.now(); LocalDate FirstDateInMonth =
	 * LocalDate.of(localDate.getYear(), localDate.getMonthValue(), 1); LocalDate
	 * tenthDateInMonth = LocalDate.of(localDate.getYear(),
	 * localDate.getMonthValue(), 10); // allowing user from month 1 Day to 10th
	 * till midnight if (FirstDateInMonth.compareTo(localDate) *
	 * localDate.compareTo(tenthDateInMonth) >= 0) generatingToken(resp,
	 * userDetails); else resp.
	 * setMsg("user is allowed to fill insurance from 1st day of month to 10th Day of Month"
	 * );
	 * 
	 * }else generatingToken(resp, userDetails);
	 * 
	 * }
	 * 
	 * responseEntity= ResponseEntity.ok(resp); }
	 * 
	 * } catch (DisabledException e) { responseEntity = ResponseEntity.ok(new
	 * UserResponse(e.getMessage())); } catch(Exception e){ responseEntity =
	 * ResponseEntity.badRequest().body(e.getMessage()); } return responseEntity; }
	 * void generatingToken(UserResponse resp, UserDetails userDetails){ final
	 * String token = jwtTokenUtil.generateToken(userDetails);
	 * resp.setUsername(userDetails.getUsername()); resp.setToken(token); }
	 */

	@RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest){
		logger.info("In AuthenticationController : createAuthenticationToken () ");
    	try{
            final Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUserName(), authenticationRequest.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
			final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUserName());
			List<GrantedAuthority> authorities = (List<GrantedAuthority>) userDetails.getAuthorities();
			System.out.println("rolename"+authorities.get(0).getAuthority());
			final String token = jwtTokenUtil.generateToken(userDetails);
			UserResponse resp = new UserResponse();
			resp.setUsername(authenticationRequest.getUserName());
			resp.setToken(token);
			resp.setIsAuthenticatedUser(true);
			resp.setAuthorities(authorities);
			return ResponseEntity.ok(resp);
        	}catch(Exception e){
        		logger.error("In AuthenticationController : createAuthenticationToken () ", e);
        		return ResponseEntity.badRequest().body(e.getMessage());
        	}
    }
    
    
	

    @RequestMapping(value = "/refresh", method = RequestMethod.GET)
    public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
    	logger.info("In AuthenticationController : refreshAndGetAuthenticationToken () ");
    	String token = null;
        try {
			 token = request.getHeader(tokenHeader);
			if (jwtTokenUtil.canTokenBeRefreshed(token/*, user.getLastPasswordResetDate()*/)) {
			    String refreshedToken = jwtTokenUtil.refreshToken(token);
			    return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken));
			} else {
			    return ResponseEntity.badRequest().body(null);
			}
		} catch (Exception e) {
			logger.error("In AuthenticationController : refreshAndGetAuthenticationToken () ", e);
		}
		return null;
    }
    
    @GetMapping("/user/logout")
    @ResponseBody
    public ResponseEntity<?> userLogout(HttpServletRequest request){
    	logger.info("In AuthenticationController : userLogout () ");
    	UserResponse d=new UserResponse();
    	try {
    		String token = request.getHeader(tokenHeader).substring(7);
			jwtTokenUtil.invalidateToken(token); 
			d.setIsAuthenticatedUser(true);
			return ResponseEntity.ok(d);
		} catch (Exception e) {
			logger.error("In AuthenticationController : userLogout () ", e);
		}
		return ResponseEntity.ok(d);
    }
    
    
    
    @GetMapping("/currentuser")
	public JwtUser getAuthenticatedUser(HttpServletRequest request) {
		return userSerivce.getAuthenticatedUser(request);
	}
    
}
