package com.aizant.ip.user;

import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;

import com.aizant.ip.security.JwtUser;


public interface IUserService {
	Boolean saveUser(User user);

	User getUser(Long id);

	Boolean deleteUser(Long id);

	Set<JwtUser> getUsers();

	Boolean updateUser(Map<String, Object> map);

	Boolean checkUserExists(String username);

	Object changeCurrentUserPassword(Long id, UserCredentials userCredentials);

	JwtUser getAuthenticatedUser(HttpServletRequest request);


	ResponseEntity<?> getUserStatus(Long id);

}
