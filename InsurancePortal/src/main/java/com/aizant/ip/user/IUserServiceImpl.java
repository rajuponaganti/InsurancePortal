package com.aizant.ip.user;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.aizant.ip.security.JwtTokenUtil;
import com.aizant.ip.security.JwtUser;
import com.aizant.ip.security.JwtUserFactory;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class IUserServiceImpl implements IUserService {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private IUserRepository userRepository;
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Value("${jwt.header}")
	private String tokenHeader;
	
	@PersistenceContext
	private EntityManager  entityManager;
	
	@Override
	public Boolean saveUser(User user) {
		logger.info("In IUserServiceImpl : saveUser () ");
		Boolean flag = false;
		try {
			
			if (user != null) {
				user.setPassword(encoder.encode(user.getPassword()));
				user.setEnabled(true);
				User saveUser = userRepository.save(user);
				flag= Objects.nonNull(saveUser);
				
			}
			
		} catch (Exception e) {
			logger.error("In IUserServiceImpl : saveUser () ", e);
		}
		return  flag;
	}

	@Override
	public Set<JwtUser> getUsers() {
		logger.info("In IUserServiceImpl : getUsers () ");
		Set<JwtUser> jwtUsers= null;
		try {
			List<User> users = userRepository.findAll();
			jwtUsers = new HashSet<JwtUser>();
			for (User user : users) {
				jwtUsers.add(JwtUserFactory.create(user));
			}
		} catch (Exception e) {
			logger.error("In IUserServiceImpl : getUsers () ", e);
		}
		return jwtUsers;
	}

	@Override
	public User getUser(Long id) {
		logger.info("In IUserServiceImpl : getUser () ");
		User user = null;
		try {
			Optional<User> optional = userRepository.findById(id);
			user = (optional.isPresent()) ? optional.get() : null;
		} catch (Exception e) {
			logger.error("In IUserServiceImpl : getUser () ", e);
		}
		return user;
	}

	@Override
	public Boolean deleteUser(Long id) {
		logger.info("In IUserServiceImpl : deleteUser () ");
		Boolean flag = false;
		try {
			Optional<User> optional = userRepository.findById(id);
			if (optional.isPresent()) {
				User user = optional.get();
				user.setEnabled(false);
				User savedUser = userRepository.save(user);
				flag = Objects.nonNull(savedUser);
			}
		} catch (Exception e) {
			logger.error("In IUserServiceImpl : deleteUser () ",e);
		}
		return flag;
	}

	@Override
	public Boolean updateUser(Map<String, Object> map) {
		logger.info("In IUserServiceImpl : updateUser () ");
		Boolean flag=false;
		try {
			if (map!=null) {
				ObjectMapper objectMapper = new ObjectMapper ();
				User user = objectMapper.convertValue(map.get("user"), User.class);
				Optional<User> optional = userRepository.findById(user.getId());
				User savedUser1 = (optional.isPresent())?optional.get():null;
				if (user.getPassword()!=null && savedUser1.getPassword()!=null && savedUser1.getPassword().length()!=user.getPassword().length()) user.setPassword(encoder.encode(user.getPassword()));
				User savedUser = userRepository.save(user);
				flag=Objects.nonNull(savedUser);

			}
		} catch (IllegalArgumentException e) {
			logger.error("In IUserServiceImpl : updateUser () ", e);
		}
		return flag;
	}

	@Override
	public Boolean checkUserExists(String username) {
		logger.info("In IUserServiceImpl : checkUserExists () ");
		Boolean flag = null;
		try {
			flag = Objects.nonNull(userRepository.findByUserName(username));
		} catch (Exception e) {
			logger.error("In IUserServiceImpl : checkUserExists () ", e);
		}
		return flag;
	}

	@Override
	public Object changeCurrentUserPassword(Long id, UserCredentials userCredentials) {
		logger.info("In IUserServiceImpl : checkUserExists () ");
		try {
			User user = userRepository.findById(id).get();
			if (userCredentials.getOldPassword() != null) {
				if (encoder.matches(userCredentials.getOldPassword(), user.getPassword())) {
					user.setPassword(encoder.encode(userCredentials.getNewPassword()));
					return Objects.nonNull(userRepository.save(user));
				} else
					return "old password is not matched with current password";
			}
		} catch (Exception e) {
			logger.error("In IUserServiceImpl : checkUserExists () ", e);
		}
		return "Password change failed";
	}

	@Override
	public JwtUser getAuthenticatedUser(HttpServletRequest request) {
		logger.info("In IUserServiceImpl : getAuthenticatedUser () ");
		JwtUser jwtUser = null ;
		try {
			String token = request.getHeader(tokenHeader).substring(7);
			String username = jwtTokenUtil.getUsernameFromToken(token);
			jwtUser = (JwtUser) userDetailsService.loadUserByUsername(username);
		} catch (UsernameNotFoundException e) {
			logger.info("In IUserServiceImpl : getAuthenticatedUser () ", e);
		}
		catch (Exception e) {
			logger.error("In IUserServiceImpl : getAuthenticatedUser () ", e);
		}
		return jwtUser;
	}
	

	
	
	@Override
	public ResponseEntity<?> getUserStatus(Long id) {
		logger.info("In IUserServiceImpl : getUserStatus () ");
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			Optional<User> optional = userRepository.findById(id);
			User user = (optional.isPresent())?optional.get():null;
			if(user!=null) map.put("userStatus", user.getEnabled());
			else map.put("isExistedUser", false);
			
		} catch (Exception e) {
			logger.error("In IUserServiceImpl : getUserStatus () ", e);
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		return ResponseEntity.ok(map);
	}

	

}
