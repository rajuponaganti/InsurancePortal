package com.aizant.ip.user;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Entity
@Table(name = "tbl_user")
@Data
public class User {
	/*
	 * @Id
	 * 
	 * @Column(name = "id")
	 * 
	 * @GeneratedValue(strategy = GenerationType.AUTO, generator = "name")
	 * 
	 * @SequenceGenerator(name = "name", sequenceName = "SEQ", allocationSize = 1,
	 * initialValue = 2)
	 */
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Long id;

	@Column(name = "username", length = 50, unique = true)
	private String userName;

	@Column(name = "password", length = 100)
	private String password;

	@Column(name = "firstname", length = 50)
	private String firstname;

	@Column(name = "lastname", length = 50)
	private String lastname;

	@Column(name = "email", length = 50)
	private String email;

	@Column(name = "mobile", length = 50)
	private String mobile;


	@Column(name = "enabled")
	private Boolean enabled = true;

	@Transient
	private String centerName;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinTable(name = "tbl_user_authority", joinColumns = {
			@JoinColumn(name = "user_id", referencedColumnName = "id") }, inverseJoinColumns = {
					@JoinColumn(name = "authority_id", referencedColumnName = "id") })
	private Set<Authority> authorities;

}
