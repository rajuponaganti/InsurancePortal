package com.aizant.ip.user;

import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.aizant.ip.security.JwtUser;


@RestController
public class UserController {

	@Autowired
	private IUserService userService;

	@PostMapping("/user")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> saveUser(@RequestBody User user) {
		return ResponseEntity.ok(userService.saveUser(user));
	}

	@GetMapping("/users")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<Set<JwtUser>> getUsers() {
		return ResponseEntity.ok(userService.getUsers());
	}

	@DeleteMapping("/user/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> deleteUser(@PathVariable Long id) {
		return ResponseEntity.ok(userService.deleteUser(id));
	}

	@PutMapping("/users")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> updateUser(@RequestBody Map<String, Object> map) {
		return ResponseEntity.ok(userService.updateUser(map));
	}

	

	@GetMapping("/user/check/{username}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> checkUserExists(@PathVariable String username) {
		return ResponseEntity.ok(userService.checkUserExists(username));
	}

	@PostMapping("/currentuser/changepassword/{id}")
	public ResponseEntity<?> changeCurrentUserPassword(@PathVariable Long id, @RequestBody UserCredentials userCredentials) {
		return ResponseEntity.ok(userService.changeCurrentUserPassword(id, userCredentials));
	}
	@GetMapping("/users/{id}")
	public ResponseEntity<?> getuser(@PathVariable Long id) {
		return ResponseEntity.ok(userService.getUser(id));
	}
	@GetMapping("/user/status/{id}")
	public ResponseEntity<?> getUserStatus(@PathVariable Long id) {
		return userService.getUserStatus(id);
	}
	
}
